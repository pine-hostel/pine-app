import React, { Component } from 'react';
import { StackNavigator, DrawerNavigator, TabNavigator, TabBarBottom } from 'react-navigation';
import { Icon } from 'react-native-elements';
import SplashScreen from '@containers/Launch/SplashScreen';
import Authenticate from '@containers/auth/AuthenticateView';
import CustomDrawer from '@containers/drawer';
import EnterOTP from '@containers/auth/EnterOTP';
import ChooseLocation from '@containers/Home/ChooseLocation';
import HostelDetail from '@containers/Home/HostelDetail'
import BirdView from '@containers/Home/BirdView'
import HostelDetailMapView from '@containers/Home/MapView'
import Listing from '@containers/Home/Listing';
import Filters from '@containers/Home/Filters'
import BookForm from '@containers/Home/BookForm'
import Profile from '@containers/profile/Profile'
import ProfileEdit from '@containers/profile/ProfileEdit'
import DismissableStackNavigator from 'src/components/DismissableStackNavigator';


const AuthRoutes = StackNavigator({
  Authenticate: { screen: Authenticate },
  EnterOTP: { screen: EnterOTP }
}, { headerMode: 'none' });

const ProfileRoutes = StackNavigator({
  Profile: { screen: Profile },
  ProfileEdit: { screen: ProfileEdit }
});

const FilterModal = DismissableStackNavigator({
  Filters: { screen: Filters },
}, {
  mode: "modal"
});

const ListingStack = StackNavigator({
  Listing: { screen: Listing },
  HostelDetail: { screen: HostelDetail },
  BirdView: { screen: BirdView },
  HostelDetailMapView: { screen: HostelDetailMapView },
  BookForm: { screen: BookForm }
}, {headerMode: 'screen'});

const Main = DrawerNavigator({
  ChooseLocation: {screen: ChooseLocation},
  Listing: { screen: ListingStack },
  ProfileStack: { screen: ProfileRoutes }
}, {
  contentComponent: CustomDrawer,
  headerMode: 'none'
});

const Routes = StackNavigator({
  SplashScreen: { screen: SplashScreen },
  Auth: { screen: AuthRoutes },
  FilterModal: { screen: FilterModal },
  Main: { screen: Main }
}, {headerMode: 'none'});

export default Routes;
