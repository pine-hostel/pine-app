import React from 'react';
import { connect, Provider } from 'react-redux';
import { Router } from 'react-native-router-flux';
import { AppStyles } from '@theme/';
import AppRoutes from '@navigation/';
import store from './store';
import Routes from './routes';

const RouterWithRedux = connect()(Router);

export default function AppContainer() {
  return (
    <Provider store={store}>
      <Routes scenes={AppRoutes} style={AppStyles.appContainer} />
    </Provider>
  );
}
