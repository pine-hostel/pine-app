/**
 * Menu Contents
 *
 * React Native Starter App
 * https://github.com/mcnamee/react-native-starter-app
 */
import React, { Component, PropTypes } from 'react';
import {
  View,
  Alert,
  StyleSheet,
  TouchableOpacity,
  Image
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import Communications from 'react-native-communications';

// Consts and Libs
import { AppStyles, AppSizes, AppColors } from '@theme/';

// Components
import { Spacer, Text, Button } from '@ui/';



/* Styles ==================================================================== */
const MENU_BG_COLOR = '#2A323A';

const white80 = "rgba(255, 255, 255, 0.8)";
const white70 = "rgba(255, 255, 255, 0.7)";
const white50 = "rgba(255, 255, 255, 0.2)";

import store from 'src/store';

import * as UserActions from '@redux/user/actions';

const styles = StyleSheet.create({
  backgroundFill: {
    backgroundColor: MENU_BG_COLOR,
    flex: 1,
    height: AppSizes.screen.height,
    width: null
  },
  profileContainer: {
    paddingTop: 32,
    paddingBottom: 16,
    borderBottomWidth: 0.3,
    borderColor: white70
  },
  menuContainer: {
    flex: 4
  },

  profileStatItem: {
    paddingLeft: 20,
    paddingRight: 20,
    alignItems: "center"
  },

  profileStatImage: {
    width: 16, height: 16
  },

  profileStatsLabel: {
    fontWeight: "100",
    fontSize: 10
  },

  profileStatsCount: {
    fontWeight: "200",
    fontSize: 16
  },

  menuTextColor: {
    color: white80
  },

  menuItem: {
    paddingLeft: 16,
    paddingTop:16,
    paddingBottom:16,
    borderBottomWidth: 0.2,
    borderColor: white50
  },

  menuItemText: {
    paddingLeft: 16,
    color: 'white',
    fontSize: 14,
    fontWeight: "300"
  },
  horizontalLine: {
    height: 30,
    borderColor: white50,
    borderRightWidth: 0.2
  }
});

/* Component ==================================================================== */
class CustomDrawer extends Component {
  constructor() {
    super();

    this.state = {
      menu: [
        {
          icon: require('@images/home.png'),
          title: 'HOME',
          onPress: () => {
            Actions.listing();
          },
        },
        {
          icon: require('@images/profile.png'),
          title: 'PROFILE',
          onPress: () => {
            Actions.profile();
          },
        },
        {
          icon: require('@images/heart_white.png'),
          title: 'FAVOURITES',
          onPress: () => {
            Actions.favourites();
          },
        },
        {
          icon: require('@images/notification.png'),
          title: 'NOTIFICATIONS',
          onPress: () => {
          },
        },
        {
          icon: require('@images/help.png'),
          title: 'HELP',
          onPress: () => {
            this.openCall();
          },
        },
        {
          icon: require('@images/logout.png'),
          title: 'LOGOUT',
          onPress: () => {
            this.logout();
          },
        },
      ],
    };
  }

  openCall() {
    Communications.phonecall("9000051535", true)
  }

  logout() {
    store.dispatch(UserActions.logout());
    Actions.authenticate();
  }

  login = () => {
  }

  render = () => {
    const { menu } = this.state;
    let user = store.getState().user;

    // Build the actual Menu Items
    const menuItems = [];
    menu.map((item) => {
      const { title, onPress } = item;

      return menuItems.push(
        <TouchableOpacity
          key={`menu-item-${title}`}
          onPress={onPress}
          activeOpacity={0.9}
        >
          <View style={[styles.menuItem, {flexDirection: 'row', alignItems: 'center'}]}>
            <Image source={item.icon} style={{width:36, height:28}} resizeMode={'contain'}/>
            <Text style={[styles.menuItemText]}>
              {title}
            </Text>
          </View>
        </TouchableOpacity>,
      );
    });

    let imageSource = user.profilePicUrl? {uri: user.profilePicUrl} : require('@images/profile_pic.jpg');

    return (
      <View style={styles.backgroundFill}>
        <View style={styles.profileContainer}>
          <View style={{flexDirection:"column", alignItems: "center", paddingLeft: 16}}>
            <Image source={imageSource}
                   style={{width:108, height:108, borderRadius: 100}} resizeMode="contain" />
            <Text
              style={[styles.menuTextColor, {fontSize:18, paddingLeft:16, fontWeight:"400"}]}>{user.firstName}</Text>
          </View>
        </View>
        <View style={styles.menuContainer}>
          {menuItems}
        </View>

      </View>
    );
  }
}

/* Export Component ==================================================================== */
export default CustomDrawer;
