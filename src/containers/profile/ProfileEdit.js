/**
 * Created by thespidy on 31/07/17.
 */
/**
 * Created by thespidy on 24/05/17.
 */
/**
 * Created by thespidy on 22/05/17.
 */
/**
 * Receipe Tabs Screen
 *  - Shows tabs, which contain receipe listings
 *
 * React Native Starter App
 * https://github.com/mcnamee/react-native-starter-app
 */
import React, { Component, PropTypes } from 'react';
import {
  View,
  StyleSheet,
  InteractionManager,
  ListView,
  Image,
  Platform,
  Text,
  StatusBar,
  TouchableOpacity,
  ScrollView
} from 'react-native';

import { Actions } from 'react-native-router-flux';

import SplashScreen from 'react-native-splash-screen'

import { connect } from 'react-redux';

import { Icon } from 'react-native-elements'
import { AppColors } from '@theme/';

import * as UserActions from '@redux/user/actions';

let t = require('tcomb-form-native');

let Form = t.form.Form;

let User = t.struct({
  firstName: t.String,              // a required string
  lastName: t.String,  // an optional string
  email: t.maybe(t.String),               // a required number
  phone: t.maybe(t.String),
  city: t.maybe(t.String),
  state: t.maybe(t.String)
});

let options = {};

const mapStateToProps = state => ({
  user: state.user
});

const mapDispatchToProps = {
  updateMe: UserActions.updateMe
}

class ProfileEdit extends Component {
  constructor(props) {
    super(props);
  };

  onSave() {
    let values = this.refs.form.getValue();
    if (values) {
      this.props.updateMe(values);
      Actions.profile({});
    }
  }

  componentDidMount() {
    SplashScreen.hide();
  }

  render = () => {
    let user = this.props.user;
    let value = {
      firstName: user.firstName,
      lastName: user.lastName,
      email: user.email,
      phone: user.phone,
      city: user.city,
      state: user.state
    }
    return (
      <View style={styles.container}>
        <Form
          ref="form"
          type={User}
          options={options}
          value={value}
        />
        <TouchableOpacity style={styles.button} onPress={() => this.onSave()} underlayColor='#99d9f4'>
          <Text style={styles.buttonText}>Save</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  coverPicContainer: {
    flex: 1,
    height: 240
  },
  textHeading: {
    color: "#9E9E9E",
    fontSize: 14,
    paddingLeft: 16,
    paddingTop: 4,
    paddingBottom: 4
  },
  textValue: {
    color: "black",
    fontSize: 14,
    paddingRight: 16,
    paddingTop: 4,
    paddingBottom: 4
  },
  profileElementContainer: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "space-between",
    paddingBottom: 8,
    paddingTop: 8,
    borderBottomWidth: 0.2,
    borderColor: "#E0E0E0"
  },
  container: {
    justifyContent: 'center',
    padding: 20,
    backgroundColor: '#ffffff',
  },
  title: {
    fontSize: 30,
    alignSelf: 'center',
    marginBottom: 30
  },
  buttonText: {
    fontSize: 18,
    color: 'white',
    alignSelf: 'center'
  },
  button: {
    height: 36,
    backgroundColor: '#48BBEC',
    borderColor: '#48BBEC',
    borderWidth: 1,
    borderRadius: 8,
    marginBottom: 10,
    alignSelf: 'stretch',
    justifyContent: 'center'
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(ProfileEdit);
