/**
 * Created by thespidy on 24/05/17.
 */
/**
 * Created by thespidy on 22/05/17.
 */
/**
 * Receipe Tabs Screen
 *  - Shows tabs, which contain receipe listings
 *
 * React Native Starter App
 * https://github.com/mcnamee/react-native-starter-app
 */
import React, { Component, PropTypes } from 'react';
import {
  View,
  StyleSheet,
  InteractionManager,
  ListView,
  Image,
  Platform,
  Text,
  StatusBar,
  TouchableOpacity,
  ScrollView
} from 'react-native';

import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import SplashScreen from 'react-native-splash-screen'

import { Icon } from 'react-native-elements'
import { AppColors } from '@theme/';

const mapStateToProps = state => ({
  user: state.user
});

class Profile extends Component {
  constructor(props) {
    super(props);
  };

  componentDidMount() {
    SplashScreen.hide();
  }

  static renderRightButton = (props) => {
    return (
      <TouchableOpacity onPress={() => Actions.profileEdit({})} activeOpacity={0.9}>
        <Icon name="edit" color="white" />
      </TouchableOpacity>
    );
  }

  render = () => {
    return (
      <View style={{flex:1, width:null, height:null}}>
        <StatusBar backgroundColor={"#0E1F3B"} />
        <View
          style={{flexDirection: "row", height:280, backgroundColor: "white", marginTop: -1 * ((Platform.OS !== 'ios' ? 54 : 64) + 8)}}>
          <Image source={require('./images/hostel_7.jpg')} style={styles.coverPicContainer}>
            <View
              style={{flex:1, flexDirection:"row", alignItems:"flex-end",justifyContent:"center", marginBottom: -30, backgroundColor:"transparent"}}>
              <View style={{borderColor: "white", width:108, height:108}}>
                <Image source={{uri: this.props.user.profilePicUrl}}
                       resizeMode='contain'
                       style={{flex:1, width:null, height:null, borderRadius: 80, zIndex:11}} />
              </View>
            </View>
          </Image>
        </View>
        <View style={{flex:1, flexDirection:"column", backgroundColor: "white"}}>
          <ScrollView showsHorizontalScrollIndicator={false}>
            <View style={styles.profileElementContainer}>
              <Text style={styles.textHeading}>Username</Text>
              <Text
                style={styles.textValue}>{this.props.user.firstName ? this.props.user.firstName : '--'} {this.props.user.lastName ? this.props.user.lastName : ''}</Text>
            </View>
            <View style={styles.profileElementContainer}>
              <Text style={styles.textHeading}>Email</Text>
              <Text style={styles.textValue}>{this.props.user.email ? this.props.user.email : '--'}</Text>
            </View>
            <View style={styles.profileElementContainer}>
              <Text style={styles.textHeading}>Phone Number</Text>
              <Text style={styles.textValue}>{this.props.user.phone ? this.props.user.phone : '--'}</Text>
            </View>
            <View style={styles.profileElementContainer}>
              <Text style={styles.textHeading}>City</Text>
              <Text style={styles.textValue}>{this.props.user.city ? this.props.user.city : '--'}</Text>
            </View>
            <View style={styles.profileElementContainer}>
              <Text style={styles.textHeading}>State</Text>
              <Text style={styles.textValue}>{this.props.user.state ? this.props.user.state : '--'}</Text>
            </View>
          </ScrollView>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  coverPicContainer: {
    flex: 1,
    height: 240
  },
  textHeading: {
    color: "#9E9E9E",
    fontSize: 14,
    paddingLeft: 16,
    paddingTop: 4,
    paddingBottom: 4
  },
  textValue: {
    color: "black",
    fontSize: 14,
    paddingRight: 16,
    paddingTop: 4,
    paddingBottom: 4
  },
  profileElementContainer: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "space-between",
    paddingBottom: 8,
    paddingTop: 8,
    borderBottomWidth: 0.2,
    borderColor: "#E0E0E0"
  }
});

export default connect(mapStateToProps, {})(Profile);
