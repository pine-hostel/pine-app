/**
 * Whole App Container
 *
 * React Native Starter App
 * https://github.com/mcnamee/react-native-starter-app
 */
import React, {Component, PropTypes} from 'react';
import {View} from 'react-native';
import {connect} from 'react-redux';
import Drawer from 'react-native-drawer'
import {DefaultRenderer} from 'react-native-router-flux';

// Consts and Libs
import {AppSizes} from '@theme/';

// Containers
import Menu from '@containers/ui/Menu/MenuContainer';

/* Redux ==================================================================== */
// Actions
import * as SideMenuActions from '@redux/sidemenu/actions';

// What data from the store shall we send to the component?
const mapStateToProps = state => ({
  sideMenuIsOpen: state.sideMenu.isOpen,
});

// Any actions to map to the component?
const mapDispatchToProps = {
  toggleSideMenu: SideMenuActions.toggle,
  closeSideMenu: SideMenuActions.close,
};

/* Component ==================================================================== */
class SideMenu extends Component {
  static componentName = 'Drawer';

  static propTypes = {
    navigationState: PropTypes.shape({}),
    onNavigate: PropTypes.func,
    sideMenuIsOpen: PropTypes.bool,
    closeSideMenu: PropTypes.func.isRequired,
    toggleSideMenu: PropTypes.func.isRequired,
  }

  static defaultProps = {
    navigationState: null,
    onNavigate: null,
    sideMenuIsOpen: null,
  }

  render() {
    const state = this.props.navigationState;
    const children = state.children;

    return (
      <Drawer
        type="overlay"
        content={<Menu closeSideMenu={this.props.closeSideMenu}
                    ref={(b) => { this.rootSidebarMenuMenu = b; }}/>}
        ref={(ref) => this._drawer = ref}
        tapToClose={true}
        onClose={() => this.props.closeSideMenu()}
        open={this.props.sideMenuIsOpen}
        openDrawerOffset={0.2} // 20% gap on the right side of drawer
        panCloseMask={0.2}
        closedDrawerOffset={-3}
        styles={drawerStyles}
        tweenHandler={(ratio) => ({
                main: { opacity:(2-ratio)/2 }
                 })}
      >
        <View style={{ backgroundColor: 'transparent', flex: 1 }}>
          <DefaultRenderer navigationState={children[0]} onNavigate={this.props.onNavigate}/>
        </View>
      </Drawer>
    );
  }
}

const drawerStyles = {
  drawer: { shadowColor: '#000000', shadowOpacity: 0.5, shadowRadius: 3},
  main: {paddingLeft: 3},
}

/* Export Component ==================================================================== */
export default connect(mapStateToProps, mapDispatchToProps)(SideMenu);
