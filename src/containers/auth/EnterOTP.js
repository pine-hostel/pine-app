/**
 * Created by thespidy on 11/07/17.
 */
/**
 * Authenticate Screen
 *  - Entry screen for all authentication
 *  - User can tap to login, forget password, signup...
 *
 * React Native Starter App
 * https://github.com/mcnamee/react-native-starter-app
 */
import React, { Component } from 'react';
import {
  View,
  Image,
  TextInput,
  Text,
  StyleSheet,
  Button,
  TouchableOpacity
} from 'react-native';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
// Actions
import * as UserActions from '@redux/user/actions';
import FacebookLogin from './FacebookLogin'
import GoogleLogin from './GoogleLogin'
import { AppStyles, AppSizes } from '@theme/';
import { Spacer } from '@ui/';
import store from 'src/store';
import SplashScreen from 'react-native-splash-screen'
import fonts from '@theme/fonts';

const appfontFamily = fonts.appfontFamily;
import Utils from '@lib/util';


const mapStateToProps = state => ({
  otp: state.user.otp
});

const mapDispatchToProps = {
  oauthLogin: UserActions.oauthLogin,
  oauthWithPhone: UserActions.oauthWithPhone
};

class EnterOTP extends Component {
  static componentName = 'Authenticate';

  constructor(props) {
    super(props);
    this.state = {};
  }

  enteredOTP = null;

  login(type, data) {
    this.props.oauthLogin(type, data, (err) => {
      if (!err) {
        this.props.navigation.navigate('Main');
      }
    });
  }

  componentDidMount() {
    SplashScreen.hide();
  }

  _enterOTPClicked() {
    if (!this.enteredOTP) {
      this.setState({ enterOTPError: "Please enter otp" });
      return;
    }
    if (!Utils.isValidOTP(this.enteredOTP)) {
      this.setState({ enterOTPError: "OTP is not valid" });
      return;
    }
    const { params } = this.props.navigation.state;
    this.props.oauthWithPhone(params.phone, this.enteredOTP);
  }

  render = () => {
    return <Image
      source={require('@images/login_bg.png')}
      style={{ flex: 1, height: null, width: null }}
    >
      <View style={{ flex: 3, width: null, height: null, justifyContent: 'center', margin: 24 }}>
        <Text
          style={{
            color: 'white',
            fontSize: 14,
            alignSelf: 'center',
            marginBottom: 24,
            fontFamily: appfontFamily.thin300,
            backgroundColor: 'transparent'
          }}>OTP has been sent to registered phone</Text>
        {this.state.enterOTPError && <Text
          style={{
            color: 'yellow',
            fontFamily: appfontFamily.thin300,
            fontSize: 12,
            backgroundColor: 'transparent'
          }}>{this.state.enterOTPError}</Text>}
        <View style={{ backgroundColor: 'transparent' }}>
          <TextInput
            underlineColorAndroid={'transparent'}
            placeholder={'Enter OTP'}
            keyboardType={'numeric'}
            placeholderTextColor={'white'}
            style={styles.loginInputContainer}
            onChangeText={text => this.enteredOTP = text}
          />
        </View>
        <TouchableOpacity onPress={() => this._enterOTPClicked()} activeOpacity={0.9}>
          <View style={{ height: 44, backgroundColor: '#FFBF25', justifyContent: 'center' }}>
            <Text
              style={{
                alignSelf: 'center',
                color: 'white',
                fontSize: 16,
                fontFamily: appfontFamily.medium500
              }}>Submit</Text>
          </View>
        </TouchableOpacity>
      </View>
    </Image>
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(EnterOTP);

const styles = StyleSheet.create({
  loginInputContainer: {
    height: 44,
    borderColor: 'white',
    borderWidth: 0.1,
    fontSize: 14,
    fontFamily: appfontFamily.thin300,
    color: 'white',
    backgroundColor: 'rgba(255,255,255,0.1)',
    marginBottom: 8,
    paddingLeft: 8
  },
  detailsContainer: {
    flex: 1,
    width: null,
    height: null,
    flexDirection: "column"
  },
  subDetails: {
    flex: 1,
    flexDirection: "column",
    justifyContent: "flex-start"
  },
  textDetails: {
    color: "white"
  }
});
