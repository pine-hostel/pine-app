/**
 * Created by thespidy on 22/05/17.
 */

import { GoogleSignin, GoogleSigninButton } from 'react-native-google-signin';

import React, { Component, PropTypes } from 'react';

import { TouchableHighlight, Image, View } from 'react-native'

export default class GoogleLogin extends Component {

  static propTypes = {
    login: PropTypes.func.isRequired,
  }

  componentDidMount() {
    this._setupGoogleSignin();
  }

  render = () => (
    <View style={{flex:1, width:null, height:42}}>
      <TouchableHighlight activeOpacity={1} onPress={()=> this._signIn()} style={{flex:1, width: null, height: 42 }}>
        <Image source={require("@images/g.png")} style={{flex:1, width:null, height:42}} />
      </TouchableHighlight>
    </View>
  )

  async _setupGoogleSignin() {
    try {
      await GoogleSignin.hasPlayServices({ autoResolve: true });
      await GoogleSignin.configure({
        webClientId: '716774868664-2regtts78qo0cg09bsbbmlvbjj6u8e94.apps.googleusercontent.com',
        offlineAccess: false
      });
    }
    catch (err) {
      console.log("Play services error", err.code, err.message);
    }
  }

  _signIn() {
    GoogleSignin.signIn()
      .then((user) => {
        let data = {
          firstName: user.name,
          email: user.email,
          accessToken: user.accessToken,
          profilePicUrl: user.photo,
          oauthId: user.id
        };
        this.props.login('google', data);
      })
      .catch((err) => {
        console.log(err);
      })
      .done();
  }

  _signOut() {
    GoogleSignin.revokeAccess().then(() => GoogleSignin.signOut()).then(() => {
      this.setState({ user: null });
    })
      .done();
  }
}

/*
 GoogleSignin.hasPlayServices({autoResolve: true}).then(() => {
 // play services are available. can now configure library
 }).catch((err) => {
 console.log("Play services error", err.code, err.message);
 });*/
