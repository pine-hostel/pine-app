/**
 * Created by thespidy on 21/05/17.
 */

import React, { Component, PropTypes } from 'react';
import {
  TouchableHighlight,
  Image,
  View
} from 'react-native';

const FBSDK = require('react-native-fbsdk');

const {
  LoginManager,
  GraphRequest,
  AccessToken,
  GraphRequestManager,
} = FBSDK;

export default class FacebookLogin extends Component {
  static propTypes = {
    login: PropTypes.func.isRequired,
  }

  onLoginClick() {
    LoginManager.logInWithReadPermissions(['public_profile']).then(
      (result) => {
        if (result.isCancelled) {
          return;
        }
        AccessToken.getCurrentAccessToken().then(
          (data) => {
            console.log('accessToken:', data.accessToken);
            const responseInfoCallback = (error, response) => {
              if (error) {
                console.log(error);
              } else {
                console.log(response);
                const userData = {
                  firstName: response.first_name,
                  lastName: response.last_name,
                  email: response.email,
                  profilePicUrl: response.picture.data.url,
                  oauthId: response.id
                };
                this.props.login('facebook', { ...userData, accessToken: data.accessToken.toString() });
              }
            };

            const infoRequest = new GraphRequest(
              '/me',
              {
                accessToken: data.accessToken,
                parameters: {
                  fields: {
                    string: 'email,name,first_name,middle_name,last_name,picture.type(large)',
                  },
                },
              },
              responseInfoCallback,
            );
            new GraphRequestManager().addRequest(infoRequest).start();
          },
        );
        console.log(result);
      },
      (error) => {
        console.log(`Login fail with error: ${error}`);
      },
    );
  }

  render = () => (
    <View style={{flex:1, width:null, height:42}} >
    <TouchableHighlight activeOpacity={1} onPress={() => this.onLoginClick() } style={{flex:1, width: null, height: 42 }}>
      <Image source={require('@images/fb.png')} style={{flex:1, width: null, height: 42 }} />
    </TouchableHighlight>
    </View>

  )
}
