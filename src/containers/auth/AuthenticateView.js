/**
 * Authenticate Screen
 *  - Entry screen for all authentication
 *  - User can tap to login, forget password, signup...
 *
 * React Native Starter App
 * https://github.com/mcnamee/react-native-starter-app
 */
import React, { Component } from 'react';
import {
  View,
  Image,
  TextInput,
  Text,
  StyleSheet,
  Button,
  TouchableOpacity
} from 'react-native';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
// Actions
import * as UserActions from '@redux/user/actions';
import FacebookLogin from './FacebookLogin'
import GoogleLogin from './GoogleLogin'
import { AppStyles, AppSizes } from '@theme/';
import { Spacer } from '@ui/';
import store from 'src/store';
import SplashScreen from 'react-native-splash-screen'
import fonts from '@theme/fonts';
import Utils from '@lib/util';
const appfontFamily = fonts.appfontFamily;


const mapStateToProps = state => ({
  isLoggedIn: state.user.isLoggedIn
});

const mapDispatchToProps = {
  oauthLogin: UserActions.oauthLogin,
  oauthWithPhone: UserActions.oauthWithPhone,
  getLoginOTP: UserActions.getLoginOTP
};

class Authenticate extends Component {
  static componentName = 'Authenticate';

  constructor(props) {
    super(props);
    this.state = {};
  }

  login(type, data) {
    this.props.oauthLogin(type, data, (err) => {
      if(!err) {
        this.props.navigation.navigate('Main');
      }
    });
  }

  phone = null

  componentDidMount() {
    SplashScreen.hide();
  }

  _loginClicked() {
    if (!this.phone) {
      this.setState({ phoneInputError: "Please enter phone number" });
      return;
    }
    if (!Utils.isValidPhone(this.phone)) {
      this.setState({ phoneInputError: "Phone number is not valid" });
      return;
    }
    this.props.getLoginOTP(this.phone);
    this.props.navigation.navigate('EnterOTP', { phone: this.phone });
  }

  render = () => {
    return <Image
      source={require('@images/login_bg.png')}
      style={{flex:1, height:null, width:null}}
    >
      <View style={{flex:3, width:null, height:null, justifyContent: 'center', margin:24}}>
        <Text
          style={{color:'white', fontSize:30, alignSelf:'center', marginBottom: 72, fontFamily: appfontFamily.thin200, backgroundColor: 'transparent'}}>Sign In</Text>
        {this.state.phoneInputError && <Text
          style={{color:'yellow', fontFamily: appfontFamily.thin300, fontSize:12, backgroundColor: 'transparent'}}>{this.state.phoneInputError}</Text>}
        <View style={{backgroundColor: 'transparent'}}>
          <TextInput
          underlineColorAndroid={'transparent'}
          placeholder={'Phone'}
          keyboardType={'numeric'}
          placeholderTextColor={'white'}
          style={styles.loginInputContainer}
          onChangeText={text => this.phone = text}
        />
        </View>

        <TouchableOpacity onPress={() => this._loginClicked()} activeOpacity={0.9}>
          <View style={{height:44, backgroundColor:'#FFBF25', justifyContent: 'center'}}>
            <Text
              style={{alignSelf: 'center', color: 'white', fontSize:16, fontFamily: appfontFamily.medium500}}>Login</Text>
          </View>
        </TouchableOpacity>
      </View>
      <Text
        style={{color:'white', fontSize:14, alignSelf:'center', backgroundColor: 'transparent'}}>Or</Text>
      <View style={{flex:1, flexDirection:'row', justifyContent:'center', margin:24}}>
        <FacebookLogin login={(type, data) => this.login(type, data)} />
        <GoogleLogin login={(type, data) => this.login(type, data)} />
      </View>
    </Image>
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Authenticate);

const styles = StyleSheet.create({
  loginInputContainer: {
    height: 44,
    borderColor: 'white',
    borderWidth: 0.1,
    fontSize: 14,
    fontFamily: appfontFamily.thin300,
    color: 'white',
    backgroundColor: 'rgba(255,255,255,0.1)',
    marginBottom: 8,
    paddingLeft: 8
  },
  detailsContainer: {
    flex: 1,
    width: null,
    height: null,
    flexDirection: "column"
  },
  subDetails: {
    flex: 1,
    flexDirection: "column",
    justifyContent: "flex-start"
  },
  textDetails: {
    color: "white"
  }
});
