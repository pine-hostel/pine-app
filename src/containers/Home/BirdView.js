/**
 * Created by thespidy on 06/08/17.
 */
import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import MapView from 'react-native-maps';
import fonts from '@theme/fonts';
const appfontFamily = fonts.appfontFamily;
import Chip from '@ui/Chip';

import {
  View,
  StyleSheet,
  InteractionManager,
  Image,
  Platform,
  StatusBar,
  Text,
  ScrollView,
  TouchableOpacity,
  TouchableHighlight,
  Dimensions
} from 'react-native';

import Rating from '@components/ui/Rating';

import { AppColors } from '@theme/';

const mapStateToProps = state => ({
  markers: state.hostels.markers
});

const mapDispatchToProps = {
};


class BirdView extends Component {
  constructor(props) {
    super(props);
  };

  getMarkers(markers) {
    let markerViews = [];
    for(let i=0; i<markers.length;i++) {
      let latlng =  {latitude: markers[i].locationCoordinates[1], longitude: markers[i].locationCoordinates[0]};
      markerViews.push(<MapView.Marker coordinate={latlng}>
        <Chip count={markers[i].count} label={markers[i].label} />
      </MapView.Marker>)
    }
    return markerViews;

  }

  render = () => {
    return (
      <View style={{flex:1, width:null, height:null, backgroundColor: '#2A323A'}}>
        <StatusBar backgroundColor={"#0E1F3B"} />
        <View style={{flex:1, width:null, height:null}}>
            <MapView
              style={{height:Dimensions.get('window').height, width:Dimensions.get('window').width}}
              initialRegion={{
                latitude: this.props.markers[0].locationCoordinates[1],
                longitude: this.props.markers[0].locationCoordinates[0],
                latitudeDelta: 0.0922,
                longitudeDelta: 0.0421,
                }}
            >
              {this.getMarkers(this.props.markers)}
            </MapView>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  textDetails: {
    color: "white"
  }
});

const mapStyle = [
  {
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#242f3e"
      }
    ]
  },
  {
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#746855"
      }
    ]
  },
  {
    "elementType": "labels.text.stroke",
    "stylers": [
      {
        "color": "#242f3e"
      }
    ]
  },
  {
    "featureType": "administrative.locality",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#d59563"
      }
    ]
  },
  {
    "featureType": "poi",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#d59563"
      }
    ]
  },
  {
    "featureType": "poi.park",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#263c3f"
      }
    ]
  },
  {
    "featureType": "poi.park",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#6b9a76"
      }
    ]
  },
  {
    "featureType": "road",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#38414e"
      }
    ]
  },
  {
    "featureType": "road",
    "elementType": "geometry.stroke",
    "stylers": [
      {
        "color": "#212a37"
      }
    ]
  },
  {
    "featureType": "road",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#9ca5b3"
      }
    ]
  },
  {
    "featureType": "road.highway",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#746855"
      }
    ]
  },
  {
    "featureType": "road.highway",
    "elementType": "geometry.stroke",
    "stylers": [
      {
        "color": "#1f2835"
      }
    ]
  },
  {
    "featureType": "road.highway",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#f3d19c"
      }
    ]
  },
  {
    "featureType": "transit",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#2f3948"
      }
    ]
  },
  {
    "featureType": "transit.station",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#d59563"
      }
    ]
  },
  {
    "featureType": "water",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#17263c"
      }
    ]
  },
  {
    "featureType": "water",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#515c6d"
      }
    ]
  },
  {
    "featureType": "water",
    "elementType": "labels.text.stroke",
    "stylers": [
      {
        "color": "#17263c"
      }
    ]
  }
];

export default connect(mapStateToProps, mapDispatchToProps)(BirdView);
