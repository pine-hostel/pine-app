import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import MapView from 'react-native-maps';
import fonts from '@theme/fonts';
const appfontFamily = fonts.appfontFamily;

import {
  View,
  StyleSheet,
  InteractionManager,
  Image,
  Platform,
  StatusBar,
  Text,
  ScrollView,
  TouchableOpacity,
  TouchableHighlight
} from 'react-native';

import { Icon } from 'react-native-elements'

import Carousel from 'react-native-looped-carousel';

import Rating from '@components/ui/Rating';

import { AppColors } from '@theme/';
import getDirections from 'react-native-google-maps-directions'
import store from 'src/store';

const mapStateToProps = state => ({});

const mapDispatchToProps = {};

class HostelDetailMapView extends Component {
  constructor(props) {
    super(props);
  };

  onDirectionsClick() {
    let location = store.getState().user.location;
    const data = {
      source: {
        latitude: location.latitude,
        longitude: location.longitude
      },
      destination: {
        latitude: this.props.pubItem.locationCoordinates[1],
        longitude: this.props.pubItem.locationCoordinates[0]
      },
      params: [
        {
          key: "dirflg",
          value: "w"
        }
      ]
    };

    getDirections(data);
  }

  bookIt() {

  }

  getCarousalImages(images) {
    let imageViews = [];
    for (image of images) {
      imageViews.push(<Image source={{uri: image}} style={{flex:1, width:null, height:240}}
                             resizeMode='cover' />);
    }
    return imageViews;
  }

  render = () => {
    let marker = {
      latlng: {latitude: this.props.hostelItem.locationCoordinates[1], longitude: this.props.hostelItem.locationCoordinates[0]},
      title: this.props.hostelItem.name
    };
    return (
      <View style={{flex:1, width:null, height:null, backgroundColor: '#2A323A'}}>
        <StatusBar backgroundColor={"#0E1F3B"} />
        <View style={{flex:1, width:null, height:null}}>
          <Image source={{uri: this.props.hostelItem.coverImageUrl}} style={{flex:1, height:250, width:null}} resizeMode={'cover'}>
            <View style={{flex:1, justifyContent:'flex-end', alignItems: 'center', margin:8}}>
              <Text style={{color:'white', fontSize: 18, backgroundColor: 'transparent' }}>{this.props.hostelItem.name}</Text>
              <Rating count={10} rating={this.props.hostelItem.rating}/>
            </View>

          </Image>
          <View style={{flex:2}}>
            <Text style={{color:'white', fontSize:12, lineHeight:20, fontFamily: appfontFamily.thin200, padding:16}}>{this.props.hostelItem.line1}, {this.props.hostelItem.locality}, {this.props.hostelItem.city}, {this.props.hostelItem.pincode}</Text>
            <MapView
              style={{height:400, width:440}}
              initialRegion={{
                latitude: marker.latlng.latitude,
                longitude: marker.latlng.longitude,
                latitudeDelta: 0.0922,
                longitudeDelta: 0.0421,
                }}
              customMapStyle={mapStyle}
            >
              <MapView.Marker
                coordinate={marker.latlng}
                title={marker.title}
              />
            </MapView>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  textDetails: {
    color: "white"
  }
});

const mapStyle = [
  {
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#242f3e"
      }
    ]
  },
  {
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#746855"
      }
    ]
  },
  {
    "elementType": "labels.text.stroke",
    "stylers": [
      {
        "color": "#242f3e"
      }
    ]
  },
  {
    "featureType": "administrative.locality",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#d59563"
      }
    ]
  },
  {
    "featureType": "poi",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#d59563"
      }
    ]
  },
  {
    "featureType": "poi.park",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#263c3f"
      }
    ]
  },
  {
    "featureType": "poi.park",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#6b9a76"
      }
    ]
  },
  {
    "featureType": "road",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#38414e"
      }
    ]
  },
  {
    "featureType": "road",
    "elementType": "geometry.stroke",
    "stylers": [
      {
        "color": "#212a37"
      }
    ]
  },
  {
    "featureType": "road",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#9ca5b3"
      }
    ]
  },
  {
    "featureType": "road.highway",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#746855"
      }
    ]
  },
  {
    "featureType": "road.highway",
    "elementType": "geometry.stroke",
    "stylers": [
      {
        "color": "#1f2835"
      }
    ]
  },
  {
    "featureType": "road.highway",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#f3d19c"
      }
    ]
  },
  {
    "featureType": "transit",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#2f3948"
      }
    ]
  },
  {
    "featureType": "transit.station",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#d59563"
      }
    ]
  },
  {
    "featureType": "water",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#17263c"
      }
    ]
  },
  {
    "featureType": "water",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#515c6d"
      }
    ]
  },
  {
    "featureType": "water",
    "elementType": "labels.text.stroke",
    "stylers": [
      {
        "color": "#17263c"
      }
    ]
  }
];

export default connect(mapStateToProps, mapDispatchToProps)(HostelDetailMapView);
