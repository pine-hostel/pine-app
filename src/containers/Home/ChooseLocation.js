/**
 * Created by thespidy on 08/07/17.
 */
/**
 * Created by thespidy on 22/05/17.
 */
/**
 * Receipe Tabs Screen
 *  - Shows tabs, which contain receipe listings
 *
 * React Native Starter App
 * https://github.com/mcnamee/react-native-starter-app
 */
import React, { Component, PropTypes } from 'react';
import {
  View,
  StyleSheet,
  InteractionManager,
  ListView,
  Image,
  Platform,
  StatusBar,
  PermissionsAndroid,
  TouchableOpacity,
  ScrollView,
  Dimensions,
  Text
} from 'react-native';

import { Icon } from 'react-native-elements'

import Carousel from 'react-native-looped-carousel';

import RNGooglePlaces from 'react-native-google-places';

import { Actions } from 'react-native-router-flux';

import * as UserActions from '@redux/user/actions';
import * as HostelActions from '@redux/hostel/actions'

import colors from '@theme/colors';

// Consts and Libs
import { AppColors } from '@theme/';
import AppAPI from '@lib/api';
import FusedLocation from 'react-native-fused-location';
import store from 'src/store'
import SplashScreen from 'react-native-splash-screen'

import fonts from '@theme/fonts';

const appfontFamily = fonts.appfontFamily;

// Containers

function getAddressObject(types, name) {
  if (types.indexOf("sublocality_level_1") > -1 || types.indexOf("sublocality")) {
    return { locality: name };
  } else if (types.indexOf("locality") > -1) {
    return { city: name };
  }
  return {}
}

class ChooseLocation extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  };

  static navigationOptions = {
    header: null
  };

  location = {};

  openSearchModal() {
    RNGooglePlaces.openAutocompleteModal({ country: 'IN' })
      .then((place) => {
        this.location = place;
        this.setState({ locationText: place.address });
        place.addressObj = getAddressObject(place.types, place.name);
        store.dispatch(HostelActions.setPlace(place));
        store.dispatch(HostelActions.getHostels());
      })
      .catch(error => console.log(error.message));
  }

  onNavigation() {
    this.props.navigation.navigate('DrawerOpen');
  }

  async componentDidMount() {
    SplashScreen.hide();
    const granted = await PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION, {
        title: 'App needs to access your location',
        message: 'App needs access to your location ' +
        'so we can let our app be even more awesome.'
      }
    );
    if (granted) {

      FusedLocation.setLocationPriority(FusedLocation.Constants.HIGH_ACCURACY);

      const location = await FusedLocation.getFusedLocation();
      this.setState({ lat: location.latitude, long: location.longitude });

      FusedLocation.setLocationPriority(FusedLocation.Constants.BALANCED);
      FusedLocation.setLocationInterval(20000);
      FusedLocation.setSmallestDisplacement(10);

      FusedLocation.startLocationUpdates();

      this.subscription = FusedLocation.on('fusedLocation', location => {
        store.dispatch(UserActions.setUserLocation(location));
      });
    }
  }

  componentWillUnmount() {
    if (this.subscription) {
      FusedLocation.off(this.subscription);
      FusedLocation.stopLocationUpdates();
    }
  }

  goToListingScreen() {
    if (Object.keys(this.location).length === 0) {
      this.setState({ enterLocationError: "Please enter location" });
      return;
    }
    this.props.navigation.navigate('Listing');
  }

  onCityClick(name) {
    store.dispatch(HostelActions.setPlace({ addressObj: { city: name } }));
    store.dispatch(HostelActions.getHostels());
    Actions.listing();
  }

  render = () => {
    let width = (Dimensions.get('window').width / 4) - 18;
    let height = width - 20;
    return (
      <View
        style={{ flex: 1, width: null, height: null }}>
        <ScrollView style={{ flexGrow: 1 }}>
          <View style={{ flex: 1, width: null, height: 180 }}>
            <Image source={require('./images/location_header.png')}
                   style={{ flex: 1, width: null, height: 180 }} resizeMode='cover'>
              <View style={{ alignItems: 'flex-start', paddingLeft:8, paddingTop:8}}>
                <TouchableOpacity onPress={() => this.onNavigation()} activeOpacity={0.9}>
                <Icon name="menu" size={32} />
                </TouchableOpacity>
              </View>
              <View style={{flex:1, justifyContent: 'center'}}>
                {this.state.enterLocationError && <Text
                  style={{
                    color: 'red',
                    fontFamily: appfontFamily.medium500,
                    fontSize: 11,
                    marginLeft: 24
                  }}>{this.state.enterLocationError}</Text>}
                <TouchableOpacity onPress={() => this.openSearchModal()} activeOpacity={0.9}>
                  <View style={{
                    flexDirection: 'row',
                    height: 36,
                    justifyContent: 'space-between',
                    alignItems: 'center',
                    marginLeft: 24,
                    marginRight: 24,
                    backgroundColor: 'rgba(0,0,0,0.6)',
                    padding: 8
                  }}>
                    <Text
                      style={{
                        color: 'white',
                        fontFamily: appfontFamily.regular400,
                        fontSize: 12
                      }}>{this.state.locationText ? this.state.locationText : 'Select Area'}</Text>
                    <Icon name='my-location' color='white' />
                  </View>
                </TouchableOpacity>
              </View>
            </Image>
          </View>
          <View style={{ flex: 1, paddingLeft: 16, paddingRight: 16 }}>
            <Text style={{ color: colors.brand.primary, marginTop: 16 }}>
              POPULAR CITIES
            </Text>
            <View style={{ borderBottomWidth: 1, borderColor: colors.brand.primary, width: 38, marginBottom: 16 }} />
            <View style={{ flex: 1, flexDirection: 'row' }}>
              <TouchableOpacity onPress={() => this.onCityClick('Chennai')} activeOpacity={0.9}>
                <View style={{ alignItems: 'center', justifyContent: 'center', marginRight: 12 }}><Image
                  source={require('./images/chennai.png')} style={{ width: width, height: height }}
                  resizeMode={'contain'} />
                  <Text style={{ fontSize: 9, color: colors.brand.primary }}>Chennai</Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity onPress={() => this.onCityClick('Bengaluru')} activeOpacity={0.9}>
                <View style={{ alignItems: 'center', justifyContent: 'center', marginRight: 12 }}><Image
                  source={require('./images/banglore.png')} style={{ width: width, height: height }}
                  resizeMode={'contain'} />
                  <Text style={{ fontSize: 9, color: colors.brand.primary }}>Bengaluru</Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity onPress={() => this.onCityClick('Hyderabad')} activeOpacity={0.9}>
                <View style={{ alignItems: 'center', justifyContent: 'center', marginRight: 12 }}><Image
                  source={require('./images/hyd.png')} style={{ width: width, height: height }}
                  resizeMode={'contain'} />
                  <Text style={{ fontSize: 9, color: colors.brand.primary }}>Hyderabad</Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity onPress={() => this.onCityClick('Vijayawada')} activeOpacity={0.9}>
                <View style={{ alignItems: 'center', justifyContent: 'center', marginRight: 12 }}><Image
                  source={require('./images/vijayawada.png')} style={{ width: width, height: height }}
                  resizeMode={'contain'} />
                  <Text style={{ fontSize: 9, color: colors.brand.primary }}>Vijayawada</Text>
                </View>
              </TouchableOpacity>
            </View>
          </View>
          <View style={{ flex: 1 }}>
            <Text style={{ color: colors.brand.primary, marginTop: 16, marginLeft: 16 }}>
              BEST OFFERS
            </Text>
            <View style={{ borderBottomWidth: 1, borderColor: colors.brand.primary, width: 38, marginLeft: 16 }} />
            <Carousel
              style={{ flex: 1, width: null, height: 130, margin: 16 }}
            >
              <Image source={require('./images/offers_1.png')} style={{ flex: 1, width: null, height: 130 }}
                     resizeMode='cover' />
              <Image source={require('./images/offers_2.png')} style={{ flex: 1, width: null, height: 130 }}
                     resizeMode='cover' />
            </Carousel>

          </View>
          <Text style={{ color: colors.brand.primary, marginTop: 16, marginLeft: 16 }}>
            WHY PINESTAYS?
          </Text>
          <View style={{ borderBottomWidth: 1, borderColor: colors.brand.primary, width: 38, marginLeft: 16 }} />
        </ScrollView>
        <TouchableOpacity onPress={() => this.goToListingScreen()} activeOpacity={0.9}>
          <View
            style={{
              height: 48,
              backgroundColor: colors.brand.primary,
              alignItems: 'center',
              justifyContent: 'center',
              elevation: 2
            }}><Text
            style={{ color: 'white' }}>SHOW PG/HOSTELS</Text></View>
        </TouchableOpacity>
      </View>
    );
  }
}

export default ChooseLocation;
