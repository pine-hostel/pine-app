/**
 * Created by thespidy on 11/07/17.
 */
/**
 * Created by thespidy on 10/07/17.
 */
/**
 * Created by thespidy on 08/07/17.
 */
/**
 * Created by thespidy on 24/05/17.
 */
/**
 * Created by thespidy on 22/05/17.
 */
/**
 * Receipe Tabs Screen
 *  - Shows tabs, which contain receipe listings
 *
 * React Native Starter App
 * https://github.com/mcnamee/react-native-starter-app
 */
import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import fonts from '@theme/fonts';
const appfontFamily = fonts.appfontFamily;

import {
  View,
  StyleSheet,
  InteractionManager,
  Image,
  Platform,
  StatusBar,
  Text,
  ScrollView,
  TouchableOpacity,
  TouchableHighlight,
  Slider,
  TextInput
} from 'react-native';

import { CheckBox } from 'react-native-elements'

import SplashScreen from 'react-native-splash-screen'

import DatePicker from 'react-native-datepicker'

import { AppColors } from '@theme/';

const mapStateToProps = state => ({});

const mapDispatchToProps = {};

class BookForm extends Component {
  constructor(props) {
    super(props);
    this.state = { checkedAgreement: false }
  };

  componentDidMount() {
    SplashScreen.hide();
  }

  render = () => {
    return (
      <View
        style={{flex:1, width:null, height:null}}>
        <StatusBar backgroundColor={"#2A323A"} />
        <ScrollView style={{flexGrow:1}}>
          <View style={{margin:16, padding:16, backgroundColor:'white'}}>
            <Text style={styles.labelText}>SELECTED HOSTEL</Text>
            <View style={{flexDirection: 'row', marginTop:8}}>
              <Image source={require('./images/hostel_7.jpg')} style={{width:64, height: 56}} />
              <View style={{marginLeft:16}}>
                <Text style={styles.subTitle}>Single Room</Text>
                <Text style={styles.content}>{this.props.hostelItem.name}</Text>
                <Text style={styles.content}>{this.props.hostelItem.locality}, {this.props.hostelItem.city}, {this.props.hostelItem.pincode}</Text>
              </View>
            </View>
          </View>
          <View style={{marginLeft:16, marginRight:16, backgroundColor:'white'}}>
            <Text style={[styles.labelText,{margin:8}]}>Start Date</Text>
            <DatePicker
              style={{width: 200, marginLeft:8}}
              date={new Date()}
              mode="date"
              placeholder="select date"
              format="YYYY-MM-DD"
              minDate={new Date()}
              maxDate="2018-06-01"
              confirmBtnText="Confirm"
              cancelBtnText="Cancel"
              customStyles={{
          dateIcon: {
            position: 'absolute',
            left: 0,
            top: 4,
            marginLeft: 0
          },
          dateInput: {
            marginLeft: 36,
            width: 64,
            padding:4,
            height:30,
            borderColor: AppColors.brand.primary,
            borderWidth: 1
          }
        }}
              onDateChange={(date) => {this.setState({date: date})}}
            />
          </View>
          <View style={{backgroundColor:'white', margin:16}}>
            <CheckBox
              title='I agree to rental terms'
              activeOpacity={0.9}
              containerStyle={{backgroundColor: 'white'}}
              onPress={()=> this.setState({checkedAgreement: !this.state.checkedAgreement})}
              checked={this.state.checkedAgreement}
            />
            <View style={{flex:1, margin:8, flexDirection: 'row'}}>
              <TextInput
                underlineColorAndroid={'transparent'}
                placeholder={'Referral/Coupon Code'}
                placeholderTextColor={'grey'}
                style={styles.referralInput}
                onChangeText={text => this.phone = text}
              />
              <TouchableOpacity onPress={()=> console.log("apply pressed")} activeOpacity={0.9}>
                <View
                  style={{flex:1, height:24 , backgroundColor: AppColors.brand.primary, alignItems:'center', justifyContent:'center', elevation:2}}><Text
                  style={{color:'white', paddingLeft:16, paddingRight:16, fontSize:12}}>APPLY</Text></View>
              </TouchableOpacity>
            </View>
          </View>
        </ScrollView>
        <TouchableOpacity onPress={()=> console.log("pressed")} activeOpacity={0.9}>
          <View
            style={{height:48 , backgroundColor: AppColors.brand.primary, alignItems:'center', justifyContent:'center', elevation:2}}><Text
            style={{alignSelf: 'center', color: 'white', fontSize:16, fontFamily: appfontFamily.medium500}}>Confirm</Text></View>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  labelText: {
    fontSize:12,
    fontFamily: appfontFamily.semiBold600,
    color: AppColors.brand.primary
  },
  subTitle: {
    fontSize:12,
    fontFamily: appfontFamily.medium500,
    color: 'black'
  },
  content: {
    fontSize: 10,
    fontFamily: appfontFamily.regular400,
    color: 'black'
  },
  referralInput: {
    flex: 2,
    height: 24,
    paddingTop: 4,
    paddingBottom: 4,
    paddingRight: 8,
    borderColor: 'grey',
    borderWidth: 0.2,
    fontSize: 11,
    fontFamily: appfontFamily.regular400,
    color: 'black',
    backgroundColor: 'white',
    paddingLeft: 8
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(BookForm);
