/**
 * Created by thespidy on 08/07/17.
 */
/**
 * Created by thespidy on 23/05/17.
 */
/**
 * Created by thespidy on 22/05/17.
 */

'use strict';

import React, { Component, PropTypes } from 'react';

import { View, Text, Image, StyleSheet, TouchableHighlight } from 'react-native'
import { Icon } from 'react-native-elements'

import { Actions } from 'react-native-router-flux';


class OrderListItem extends Component {

  constructor(props) {
   super(props);
   this.state = {isFavourite: props.hostelItem.isFavourite};
  }

  onFavourite(isRemove) {
    if(isRemove) {
      this.setState({isFavourite: false});
    } else {
      this.setState({isFavourite: true});
    }
    this.props.addToFavourite(this.props.hostelItem._id);
  }

  render() {

    let src;
    return (
      <TouchableHighlight
        onPress={() => Actions.hostelDetails({hostelItem: this.props.hostelItem})}>
        <View style={styles.itemContainer}>
          <Image source={{uri: this.props.hostelItem.coverImageUrl}} style={{flex:1, width:null, height: 180}} resizeMode={'cover'}>
            <View style={{flex:1, width:null, height:null, flexDirection:'column-reverse', justifyContent:'space-between'}}>
              <View
                style={{backgroundColor:'rgba(42,50,58,0.7)', height:44, width:null, flexDirection:'row', justifyContent:'space-between', padding:8}}>
                <View>
                  <Text style={[styles.textDetails, {fontSize:13}]}>
                    {this.props.hostelItem.name}
                  </Text>
                  <View style={{flexDirection:'row'}}>
                    <Icon name={'star'} color={'#F25C29'} size={12} /><Text style={[styles.textDetails, {fontSize:12}]}>{this.props.hostelItem.rating}</Text>
                  </View>
                </View>
                <View style={{flexDirection:'row', alignItems:'center', justifyContent:'flex-end'}}>
                  <Text style={[styles.textDetails, {fontSize:12, paddingRight:2}]}>{this.props.hostelItem.locality}</Text>
                  <Icon name={'location-on'} color={'#F25C29'} size={16} />
                </View>
              </View>
              <View style={{flex:1, flexDirection:'row', alignItems:'flex-start', justifyContent:'flex-end', padding:8}}>
                {(this.state.isFavourite || this.props.isFavourite) && <Icon onPress={() => this.onFavourite(true)} underlayColor={'transparent'} name={'favorite'} color={'red'} size={20} />}
                {(!this.state.isFavourite && !this.props.isFavourite) && <Icon onPress={() => this.onFavourite(false)} underlayColor={'transparent'} name={'favorite-border'} color={'#F5F5F5'} size={20} />}
              </View>
            </View>
          </Image>
        </View>
      </TouchableHighlight>
    );
  }
}

export default OrderListItem;

var styles = StyleSheet.create({
  itemContainer: {
    flex: 1,
    width: null,
    height: 180,
    marginBottom: 8
  },
  detailsContainer: {
    flex: 1,
    flexDirection: "column-reverse",
    justifyContent: "flex-start",
  },
  textDetails: {
    color: "white"
  }
});