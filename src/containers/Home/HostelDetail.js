/**
 * Created by thespidy on 08/07/17.
 */
/**
 * Created by thespidy on 24/05/17.
 */
/**
 * Created by thespidy on 22/05/17.
 */
/**
 * Receipe Tabs Screen
 *  - Shows tabs, which contain receipe listings
 *
 * React Native Starter App
 * https://github.com/mcnamee/react-native-starter-app
 */
import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import colors from '@theme/colors';

import {
  View,
  StyleSheet,
  InteractionManager,
  Image,
  Platform,
  StatusBar,
  Text,
  ScrollView,
  TouchableOpacity,
  TouchableHighlight
} from 'react-native';

import { Icon } from 'react-native-elements'

import Carousel from 'react-native-looped-carousel';

import Rating from '@components/ui/Rating';

import { AppColors } from '@theme/';
import getDirections from 'react-native-google-maps-directions'
import store from 'src/store';
import SplashScreen from 'react-native-splash-screen'

import fonts from '@theme/fonts';
const appfontFamily = fonts.appfontFamily;

const mapStateToProps = state => ({});

const mapDispatchToProps = {};

class HostelDetail extends Component {
  constructor(props) {
    super(props);
  };

  onDirectionsClick() {
    let location = store.getState().user.location;
    const data = {
      source: {
        latitude: location.latitude,
        longitude: location.longitude
      },
      destination: {
        latitude: this.props.pubItem.locationCoordinates[1],
        longitude: this.props.pubItem.locationCoordinates[0]
      },
      params: [
        {
          key: "dirflg",
          value: "w"
        }
      ]
    };

    getDirections(data);
  }

  static renderRightButton = (props) => {
    return (
      <TouchableOpacity onPress={() => Actions.hostelDetailMap({hostelItem: props.hostelItem})} activeOpacity={0.9}>
        <Icon name="location-on" color="white" />
      </TouchableOpacity>
    );
  }

  bookIt() {
    Actions.bookForm({hostelItem: this.props.hostelItem});
  }

  componentDidMount() {
    SplashScreen.hide();
  }

  getCarousalImages(images) {
    let imageViews = [];
    for (image of images) {
      imageViews.push(<Image source={{uri: image}} style={{flex:1, width:null, height:350}}
                             resizeMode='cover' />);
    }
    return imageViews;
  }

  render = () => {
    return (
      <Image style={{flex:1, width:null, height:null}} source={require('./images/details_bg.png')}>
        <StatusBar backgroundColor={"#0E1F3B"} />

        <View style={{flex:1, width:null, height:null}}>
          <ScrollView style={{flexGrow:1}}>
            <View style={{flex:1}}>
              <Carousel
                delay={20000}
                style={styles.carousel}
                autoplay
                bullets
                bulletStyle={styles.circle}
                chosenBulletStyle={styles.selectedCircle}
                onAnimateNextPage={(p) => console.log(p)}
              >
                {this.getCarousalImages(this.props.hostelItem.imageUrls)}
              </Carousel>
            </View>
            <View style={{flex:1, margin:12}}>
              <View
                style={{flex:1, alignItems:'flex-end', justifyContent:'flex-end', flexDirection:'row', padding:8}}><Text
                style={[{fontSize:16, color: 'green', backgroundColor: 'transparent' }]}>{this.props.hostelItem.startsFrom}/</Text><Text style={[styles.whiteFont, {fontSize:12}]}>month</Text></View>
              <Rating rating={this.props.hostelItem.rating} count={20} />
              <View style={{flexDirection:'row', alignItems:'center', marginTop:16, marginBottom:18}}>
                <Icon name="location-on" color="white" size={14} />
                <Text style={[styles.whiteFont, styles.content]}>{this.props.hostelItem.locality}, {this.props.hostelItem.city}, {this.props.hostelItem.pincode}</Text>
              </View>
              <Text style={[styles.titleText]}>Hostel Description</Text>
              <Text style={[styles.whiteFont, styles.description]}>{this.props.hostelItem.description}</Text>
              <View style={{marginTop:18}}>
                <Text style={[styles.titleText]}>Amenities</Text>
                <View style={{flexDirection: 'row', justifyContent:'space-between'}}>
                  <View style={styles.amenityContainer}>
                    <Image source={require('./images/wifi.png')} style={styles.amenityIcon} resizeMode={'contain'} />
                    <Text style={styles.amenityText}>Wifi</Text>
                  </View>
                  <View style={styles.amenityContainer}>
                    <Image source={require('./images/fitness.png')} style={styles.amenityIcon} resizeMode={'contain'} />
                    <Text style={styles.amenityText}>Gym</Text>
                  </View>
                </View>
                <View style={{flexDirection: 'row', justifyContent:'space-between'}}>
                  <View style={styles.amenityContainer}>
                    <Image source={require('./images/parking.png')} style={styles.amenityIcon} resizeMode={'contain'} />
                    <Text style={styles.amenityText}>Parking</Text>
                  </View>
                  <View style={styles.amenityContainer}>
                    <Image source={require('./images/restaurant.png')} style={styles.amenityIcon} resizeMode={'contain'} />
                    <Text style={styles.amenityText}>Food</Text>
                  </View>
                </View>
                <View style={{flexDirection: 'row', justifyContent:'space-between'}}>
                  <View style={styles.amenityContainer}>
                    <Image source={require('./images/room.png')} style={styles.amenityIcon} resizeMode={'contain'} />
                    <Text style={styles.amenityText}>Room Service</Text>
                  </View>
                </View>
              </View>
            </View>
          </ScrollView>
          <View>
            <TouchableOpacity onPress={()=> this.bookIt()} activeOpacity={0.9}>
              <View
                style={{height:48 , backgroundColor: colors.brand.primary, alignItems:'center', justifyContent:'center'}}><Text
                style={{alignSelf: 'center', color: 'white', fontSize:16, fontFamily: appfontFamily.medium500}}>Book It</Text></View>
            </TouchableOpacity>
          </View>
        </View>
      </Image>
    );
  }
}

const styles = StyleSheet.create({
  carousel: {
    height: 350,
    marginTop: -1 * ((Platform.OS !== 'ios' ? 54 : 64) + 8)
  },
  selectedCircle: {
    width: 5,
    height: 5,
    borderRadius: 2.5,
    marginLeft:4,
    backgroundColor: 'white'
  },
  circle: {
    width: 5,
    height: 5,
    borderRadius: 2.5,
    borderWidth:0.4,
    borderColor:'white',
    marginLeft:4,
    backgroundColor: 'transparent'
  },
  detailsContainer: {
    flex: 1,
    width: null,
    height: null,
    flexDirection: "column"
  },
  content: {
    fontSize: 12,
    fontFamily: appfontFamily.regular400
  },
  description: {
    fontSize: 10,
    fontFamily: appfontFamily.regular400,
    lineHeight: 20
  },
  titleText: {
    backgroundColor: 'transparent',
    fontSize: 14,
    fontFamily: appfontFamily.medium500,
    color: 'white',
    borderBottomWidth: 1,
    borderColor: 'orange',
    alignSelf: 'flex-start',
    marginBottom: 8
  },
  subDetails: {
    flex: 1,
    flexDirection: "column",
    justifyContent: "flex-start"
  },
  whiteFont: {
    backgroundColor: 'transparent',
    color: "white"
  },
  amenityIcon: {
    width: 24,
    height: 24
  },
  amenityText: {
    backgroundColor: 'transparent',
    fontSize: 12,
    fontFamily: appfontFamily.regular400,
    paddingLeft:8,
    color: 'white'
  },
  amenityContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom:8,
    marginLeft:8,
    marginRight:8
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(HostelDetail);
