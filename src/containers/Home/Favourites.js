/**
 * Created by thespidy on 06/08/17.
 */
/**
 * Created by thespidy on 28/06/17.
 */
import React, { Component } from 'react';
import {
  View,
  Image,
  StyleSheet,
  ListView,
  TouchableOpacity
} from 'react-native';
import { connect } from 'react-redux';
import  { Icon } from 'react-native-elements'
import * as FavouriteActions from '@redux/favourite/actions'
import { AppStyles, AppSizes } from '@theme/';
import { Spacer, Text, Button } from '@ui/';
import HostelItem from './HostelItem';
import Utils from '@lib/util';
import SplashScreen from 'react-native-splash-screen'
import ActionButton from 'react-native-action-button';
import {Actions} from 'react-native-router-flux';

const mapStateToProps = state => ({
  favourites: state.favourites
});

const mapDispatchToProps = {
  getFavourites: FavouriteActions.getFavourites,
  addToFavourite: FavouriteActions.addToFavourite
};

class Favourites extends Component {

  ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1.name !== r2.name });
  directionDataFetchStarted = false;

  constructor(props) {
    super(props);
    props.getFavourites();
    this.state = {
      dataSource: this.ds.cloneWithRows(props.favourites.data)
    }
  };

  componentWillReceiveProps(nextProps) {
    this.setState({
      dataSource: this.ds.cloneWithRows(nextProps.favourites.data)
    })
  }

  componentDidMount() {
    SplashScreen.hide();
  }

  render = () => {
    let { isLoading, error } = this.props.favourites;
    if (isLoading) return <Loading />;
    if (error) return <Error text={error} />;
    return (
      <View style={{flex:1, width:null, height:null, backgroundColor:'#2A323A'}}>
        <ListView
          enableEmptySections={true}
          style={{paddingLeft:16, paddingRight:16}}
          renderRow={hostel => <HostelItem hostelItem={hostel} isFavourite={true} addToFavourite={this.props.addToFavourite} />}
          dataSource={this.state.dataSource}
          automaticallyAdjustContentInsets={false}
        />
      </View>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Favourites);