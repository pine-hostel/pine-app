/**
 * Created by thespidy on 28/06/17.
 */
import React, { Component } from 'react';
import {
  View,
  Image,
  StyleSheet,
  ListView,
  TouchableOpacity
} from 'react-native';
import { connect } from 'react-redux';
import  { Icon } from 'react-native-elements'
import * as HostelActions from '@redux/hostel/actions'
import { AppStyles, AppSizes } from '@theme/';
import { Spacer, Text, Button } from '@ui/';
import HostelItem from './HostelItem';
import Utils from '@lib/util';
import SplashScreen from 'react-native-splash-screen'
import ActionButton from 'react-native-action-button';
import {Actions} from 'react-native-router-flux';
import * as FavouriteActions from '@redux/favourite/actions'

const mapStateToProps = state => ({
  hostels: state.hostels
});

const mapDispatchToProps = {
  getHostels: HostelActions.getHostels,
  setDistanceInHostels: HostelActions.setDistanceInHostels,
  setFilters: HostelActions.setFilters,
  addToFavourite: FavouriteActions.addToFavourite
};

class Listing extends Component {

  ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1.name !== r2.name });
  directionDataFetchStarted = false;

  constructor(props) {
    super(props);
    let location = { latitude: props.hostels.place.latitude, longitude: props.hostels.place.longitude };
    this.getDistances(location, props.hostels.data);
    this.state = {
      dataSource: this.ds.cloneWithRows(props.hostels.data)
    }
  };

  static navigationOptions = ({ navigation }) => ({
    headerStyle: { backgroundColor: '#2A323A'},
    headerRight: <TouchableOpacity onPress={() => navigation.navigate('BirdView')} activeOpacity={0.9}>
      <Icon name="map" color="white" />
    </TouchableOpacity>,
    headerLeft: <TouchableOpacity onPress={() => navigation.navigate('DrawerOpen')} activeOpacity={0.9}>
      <Icon name="menu" size={32} color="white"/>
    </TouchableOpacity>
  });

  getDistances(location, hostels) {
    if (location && hostels.length > 0 && !this.directionDataFetchStarted) {
      let source = [location.latitude, location.longitude];
      let destinations = [];
      for (hostel of hostels) {
        destinations.push([hostel.locationCoordinates[1], hostel.locationCoordinates[0]]);
      }
      this.directionDataFetchStarted = true;
      //Utils.getDistanceAndTime(source, destinations, this.props.setDistanceInHostels)
    }
  }

  static renderRightButton = (props) => {
    return (
      <TouchableOpacity onPress={() => Actions.birdView()} activeOpacity={0.9}>
        <Icon name="map" color="white" />
      </TouchableOpacity>
    );
  };

  componentWillReceiveProps(nextProps) {
    this.setState({
      dataSource: this.ds.cloneWithRows(nextProps.hostels.data)
    });
  }

  componentDidMount() {
    SplashScreen.hide();
  }

  render = () => {
    let { isLoading, error } = this.props.hostels;
    if (isLoading) return <Loading />;
    if (error) return <Error text={error} />;
    return (
      <View style={{flex:1, width:null, height:null, backgroundColor:'#2A323A'}}>
        <ListView
          enableEmptySections={true}
          style={{paddingLeft:16, paddingRight:16}}
          renderRow={hostel => <HostelItem hostelItem={hostel} addToFavourite={this.props.addToFavourite}/>}
          dataSource={this.state.dataSource}
          automaticallyAdjustContentInsets={false}
        />
        <ActionButton
          buttonColor="rgba(231,76,60,1)"
          icon={<Icon name='filter-list' color={'white'}/>}
          onPress={() => { this.props.navigation.navigate('FilterModal', {initialFilters: this.props.hostels.filters, setFilters: this.props.setFilters})}}
        />
      </View>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Listing);