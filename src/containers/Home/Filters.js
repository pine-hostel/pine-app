/**
 * Created by thespidy on 10/07/17.
 */
/**
 * Created by thespidy on 08/07/17.
 */
/**
 * Created by thespidy on 24/05/17.
 */
/**
 * Created by thespidy on 22/05/17.
 */
/**
 * Receipe Tabs Screen
 *  - Shows tabs, which contain receipe listings
 *
 * React Native Starter App
 * https://github.com/mcnamee/react-native-starter-app
 */
import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';

import {
  View,
  StyleSheet,
  InteractionManager,
  Image,
  Platform,
  StatusBar,
  Text,
  ScrollView,
  TouchableOpacity,
  TouchableHighlight,
  Slider
} from 'react-native';

import fonts from '@theme/fonts';

const appfontFamily = fonts.appfontFamily;

import SplashScreen from 'react-native-splash-screen'

import { Icon } from 'react-native-elements'

import { AppColors } from '@theme/';

import colors from '@theme/colors'

import store from 'src/store';

import * as HostelActions from '@redux/hostel/actions'

const mapStateToProps = state => ({
  filters: state.hostels.filters,
});

const mapDispatchToProps = {
  setFilters: HostelActions.setFilters,
  resetFilters: HostelActions.resetFilters
};

class Filters extends Component {

  previousFilters = {};

  constructor(props) {
    super(props);
  }

  selectionDisplay = {
    gender: { men: {}, women: {}, couple: {} },
    bedType: { single: {}, double: {}, triple: {} },
    furnishedType: { fully: {}, semi: {}, unfurnished: {} },
    accomodationType: { none: {}, ac: {} }
  };

  static navigationOptions = ({ navigation }) => ({
    headerStyle: { backgroundColor: colors.brand.primary},
    headerTitleStyle: {
      color: '#fff',
      alignSelf: 'center'
    },
    headerTitle: 'Filters',
    headerRight: <TouchableOpacity onPress={() => {
      store.dispatch(HostelActions.getHostels(store.getState().hostels.filters));
      navigation.navigate('Listing');
    }} activeOpacity={0.9}>
      <Icon name="done" color="white" />
    </TouchableOpacity>,
    headerLeft: <TouchableOpacity onPress={() => {
      const { params } = navigation.state;
      HostelActions.setFilters(params.initialFilters);
      navigation.navigate('Listing');
    }} activeOpacity={0.9}>
      <Icon name="clear" color="white" />
    </TouchableOpacity>
  });

  componentDidMount() {
    SplashScreen.hide();
  }

  classNameAllocation() {
    let selection = this.props.filters;
    this.setGenderDisplay(selection.tenantType_in);
    this.setBedTypeDisplay(selection.bedType_in);
    this.setFurnitureType(selection.furnishingType_in);
    this.setAccomodationType(selection.accomodationType_in)
  }

  setGenderDisplay(genders) {
    this.selectionDisplay.gender.men.imageUrl = require('./images/men.png');
    this.selectionDisplay.gender.men.textClass = styles.optionDeselect;
    this.selectionDisplay.gender.women.imageUrl = require('./images/women.png');
    this.selectionDisplay.gender.women.textClass = styles.optionDeselect;
    this.selectionDisplay.gender.couple.imageUrl = require('./images/composite_grey.png');
    this.selectionDisplay.gender.couple.textClass = styles.optionDeselect;
    if (!genders || genders.length === 0) {
      return;
    }
    for (let gender of genders) {
      switch (gender) {
        case 'men':
          this.selectionDisplay.gender.men.imageUrl = require('./images/men_select.png');
          this.selectionDisplay.gender.men.textClass = styles.optionSelect;
          break;
        case 'women':
          this.selectionDisplay.gender.women.imageUrl = require('./images/women_select.png');
          this.selectionDisplay.gender.women.textClass = styles.optionSelect;
          break;
        case 'couple':
          this.selectionDisplay.gender.couple.imageUrl = require('./images/composite.png');
          this.selectionDisplay.gender.couple.textClass = styles.optionSelect;
          break;
        default:
      }
    }
  }

  setBedTypeDisplay(bedTypes) {
    this.selectionDisplay.bedType.single.imageUrl = require('./images/bed_grey.png');
    this.selectionDisplay.bedType.single.textClass = styles.optionDeselect;
    this.selectionDisplay.bedType.double.imageUrl = require('./images/double_bed_grey.png');
    this.selectionDisplay.bedType.double.textClass = styles.optionDeselect;
    this.selectionDisplay.bedType.triple.imageUrl = require('./images/double_bed_grey.png');
    this.selectionDisplay.bedType.triple.textClass = styles.optionDeselect;
    for (let bedType of bedTypes) {
      switch (bedType) {
        case 'single':
          this.selectionDisplay.bedType.single.imageUrl = require('./images/single_bed.png');
          this.selectionDisplay.bedType.single.textClass = styles.optionSelect;
          break;
        case 'double':
          this.selectionDisplay.bedType.double.imageUrl = require('./images/double_bed.png');
          this.selectionDisplay.bedType.double.textClass = styles.optionSelect;
          break;
        case 'triple':
          this.selectionDisplay.bedType.triple.imageUrl = require('./images/double_bed.png');
          this.selectionDisplay.bedType.triple.textClass = styles.optionSelect;
          break;
      }
    }
  }

  setAccomodationType(accomodationTypes) {
    this.selectionDisplay.accomodationType.ac.imageUrl = require('./images/air_conditioner.png');
    this.selectionDisplay.accomodationType.ac.textClass = styles.optionDeselect;
    this.selectionDisplay.accomodationType.none.imageUrl = require('./images/fan_deselect.png');
    this.selectionDisplay.accomodationType.none.textClass = styles.optionDeselect;
    for (let accomodationType of accomodationTypes) {
      switch (accomodationType) {
        case 'ac':
          this.selectionDisplay.accomodationType.ac.imageUrl = require('./images/air_conditioner_select.png');
          this.selectionDisplay.accomodationType.ac.textClass = styles.optionSelect;
          break;
        case 'none':
          this.selectionDisplay.accomodationType.none.imageUrl = require('./images/fan_select.png');
          this.selectionDisplay.accomodationType.none.textClass = styles.optionSelect;
          break;
      }
    }
  }

  setFurnitureType(furnishedTypes) {
    this.selectionDisplay.furnishedType.fully.imageUrl = require('./images/sofa_grey.png');
    this.selectionDisplay.furnishedType.fully.textClass = styles.optionDeselect;
    this.selectionDisplay.furnishedType.semi.imageUrl = require('./images/dining_table_grey.png');
    this.selectionDisplay.furnishedType.semi.textClass = styles.optionDeselect;
    this.selectionDisplay.furnishedType.unfurnished.imageUrl = require('./images/brick_grey.png');
    this.selectionDisplay.furnishedType.unfurnished.textClass = styles.optionDeselect;
    for (let furnishedType of furnishedTypes) {
      switch (furnishedType) {
        case 'fully':
          this.selectionDisplay.furnishedType.fully.imageUrl = require('./images/sofa_select.png');
          this.selectionDisplay.furnishedType.fully.textClass = styles.optionSelect;
          break;
        case 'semi':
          this.selectionDisplay.furnishedType.semi.imageUrl = require('./images/dining_table_select.png');
          this.selectionDisplay.furnishedType.semi.textClass = styles.optionSelect;
          break;
        case 'unfurnished':
          this.selectionDisplay.furnishedType.unfurnished.imageUrl = require('./images/brick_select.png');
          this.selectionDisplay.furnishedType.unfurnished.textClass = styles.optionSelect;
          break;
        default:
      }
    }
  }

  addToFilter(filterType, value) {
    let filters = this.props.filters;
    if (!filters[filterType]) {
      filters[filterType] = [value];
      return { ...filters };
    }
    for (let i in filters[filterType]) {
      if (filters[filterType][i] === value) {
        filters[filterType].splice(i, 1);
        return { ...filters };
      }
    }
    filters[filterType].push(value);
    return { ...filters };
  }

  render = () => {
    this.classNameAllocation();
    return (
      <View style={{ flex: 1, width: null, height: null, backgroundColor: 'white' }}>
        <StatusBar backgroundColor={"#0E1F3B"} />
        <View style={{ flex: 1, width: null, height: null }}>
          <ScrollView style={{ flexGrow: 1 }}>
            <Text style={[{ marginLeft: 16, marginRight: 16, marginTop: 18 }, styles.titleText]}>Tenant Type</Text>
            <View style={{ flexDirection: 'row', justifyContent: 'space-between', margin: 16 }}>
              <TouchableOpacity activeOpacity={0.9}
                                onPress={() => this.props.setFilters(this.addToFilter('tenantType_in', 'men'))}>
                <View style={styles.optionContainer}>
                  <Image source={this.selectionDisplay.gender.men.imageUrl} style={styles.genderImage}
                         resizeMode={'contain'} />
                  <Text style={this.selectionDisplay.gender.men.textClass}>Men</Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity activeOpacity={0.9}
                                onPress={() => this.props.setFilters(this.addToFilter('tenantType_in', 'women'))}>
                <View style={styles.optionContainer}>
                  <Image source={this.selectionDisplay.gender.women.imageUrl} style={styles.genderImage}
                         resizeMode={'contain'} />
                  <Text style={this.selectionDisplay.gender.women.textClass}>Women</Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity activeOpacity={0.9}
                                onPress={() => this.props.setFilters(this.addToFilter('tenantType_in', 'couple'))}>
                <View style={styles.optionContainer}>
                  <Image source={this.selectionDisplay.gender.couple.imageUrl} style={styles.genderImage}
                         resizeMode={'contain'} />
                  <Text style={this.selectionDisplay.gender.couple.textClass}>Couple</Text>
                </View>
              </TouchableOpacity>
            </View>
            <View style={{ borderBottomWidth: 0.2, borderColor: 'grey', marginBottom: 18 }} />
            <Text style={[{ marginLeft: 16, marginRight: 16 }, styles.titleText]}>Room Sharing</Text>
            <View style={{ flexDirection: 'row', justifyContent: 'space-between', margin: 16 }}>
              <TouchableOpacity activeOpacity={0.9}
                                onPress={() => this.props.setFilters(this.addToFilter('bedType_in', 'single'))}>
                <View style={styles.optionContainer}>
                  <Image source={this.selectionDisplay.bedType.single.imageUrl} style={styles.bedImage} />
                  <Text style={this.selectionDisplay.bedType.single.textClass}>Single</Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity activeOpacity={0.9}
                                onPress={() => this.props.setFilters(this.addToFilter('bedType_in', 'double'))}>
                <View style={styles.optionContainer}>
                  <Image source={this.selectionDisplay.bedType.double.imageUrl} style={styles.bedImage} />
                  <Text style={this.selectionDisplay.bedType.double.textClass}>Double</Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity activeOpacity={0.9}
                                onPress={() => this.props.setFilters(this.addToFilter('bedType_in', 'triple'))}>
                <View style={styles.optionContainer}>
                  <Image source={this.selectionDisplay.bedType.triple.imageUrl} style={styles.bedImage} />
                  <Text style={this.selectionDisplay.bedType.triple.textClass}>3 and more</Text>
                </View>
              </TouchableOpacity>
            </View>
            <View style={{ borderBottomWidth: 0.2, borderColor: 'grey', marginBottom: 18 }} />
            <Text style={[{ marginLeft: 16, marginRight: 16 }, styles.titleText]}>Furnishing Type</Text>
            <View style={{ flexDirection: 'row', justifyContent: 'space-between', margin: 16 }}>
              <TouchableOpacity activeOpacity={0.9}
                                onPress={() => this.props.setFilters(this.addToFilter('furnishingType_in', 'fully'))}>
                <View style={styles.optionContainer}>
                  <Image source={this.selectionDisplay.furnishedType.fully.imageUrl} style={styles.furnitureImage} />
                  <Text style={this.selectionDisplay.furnishedType.fully.textClass}>Full Furnished</Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity activeOpacity={0.9}
                                onPress={() => this.props.setFilters(this.addToFilter('furnishingType_in', 'semi'))}>
                <View style={styles.optionContainer}>
                  <Image source={this.selectionDisplay.furnishedType.semi.imageUrl} style={styles.furnitureImage} />
                  <Text style={this.selectionDisplay.furnishedType.semi.textClass}>Semi</Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity activeOpacity={0.9}
                                onPress={() => this.props.setFilters(this.addToFilter('furnishingType_in', 'unfurnished'))}>
                <View style={styles.optionContainer}>
                  <Image source={this.selectionDisplay.furnishedType.unfurnished.imageUrl}
                         style={styles.furnitureImage} />
                  <Text style={this.selectionDisplay.furnishedType.unfurnished.textClass}>Unfurnished</Text>
                </View>
              </TouchableOpacity>
            </View>
            <View style={{ borderBottomWidth: 0.2, borderColor: 'grey', marginBottom: 18 }} />
            <Text style={[{ marginLeft: 16, marginRight: 16 }, styles.titleText]}>Accomodation Type</Text>
            <View style={{ flexDirection: 'row', margin: 16 }}>
              <TouchableOpacity activeOpacity={0.9}
                                onPress={() => this.props.setFilters(this.addToFilter('accomodationType_in', 'none'))}>
                <View style={[styles.optionContainer, { marginRight: 32 }]}>
                  <Image source={this.selectionDisplay.accomodationType.none.imageUrl} style={styles.furnitureImage} />
                  <Text style={this.selectionDisplay.accomodationType.none.textClass}>Non AC</Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity activeOpacity={0.9}
                                onPress={() => this.props.setFilters(this.addToFilter('accomodationType_in', 'ac'))}>
                <View style={styles.optionContainer}>
                  <Image source={this.selectionDisplay.accomodationType.ac.imageUrl} style={styles.furnitureImage} />
                  <Text style={this.selectionDisplay.accomodationType.ac.textClass}>AC</Text>
                </View>
              </TouchableOpacity>
            </View>
            <View style={{ borderBottomWidth: 0.2, borderColor: 'grey', marginBottom: 18 }} />
            <Text style={[{ marginLeft: 16, marginRight: 16 }, styles.titleText]}>Price Range</Text>
            <View style={{ flexDirection: 'row', margin: 16, alignItems: 'center' }}>
              <Text style={styles.sliderText}>3000</Text>
              <Slider
                style={{ flex: 1, padding: 0 }}
                minimumValue={3000}
                maximumValue={20000}
                value={this.props.filters.price}
                thumbTintColor={'orange'}
                minimumTrackTintColor={'orange'}
                maximumTrackTintColor={'orange'}
                onValueChange={(value) => this.selectionDisplay.price = value} />
              <Text style={styles.sliderText}>20000</Text>
            </View>
            <View style={{ borderBottomWidth: 0.2, borderColor: 'grey', marginBottom: 18 }} />
            <Text style={[{ marginLeft: 16, marginRight: 16 }, styles.titleText]}>Rating</Text>
            <View style={{ flexDirection: 'row', margin: 16, alignItems: 'center' }}>
              <Text style={styles.sliderText}>1</Text>
              <Slider
                style={{ flex: 1, padding: 0 }}
                minimumValue={1}
                maximumValue={5}
                step={1}
                value={this.props.filters.rating}
                thumbTintColor={'orange'}
                minimumTrackTintColor={'orange'}
                maximumTrackTintColor={'orange'}
                onValueChange={(value) => this.selectionDisplay.rating = value} />
              <Text style={styles.sliderText}>5</Text>
            </View>
            <View style={{ borderBottomWidth: 0.2, borderColor: 'grey', marginBottom: 18 }} />
            <Text style={[{ marginLeft: 16, marginRight: 16 }, styles.titleText]}>Distance</Text>
            <View style={{ flexDirection: 'row', margin: 16, alignItems: 'center' }}>
              <Text style={styles.sliderText}>1km</Text>
              <Slider
                style={{ flex: 1, padding: 0 }}
                minimumValue={1}
                maximumValue={30}
                value={this.props.filters.distance}
                thumbTintColor={'orange'}
                minimumTrackTintColor={'orange'}
                maximumTrackTintColor={'orange'}
                onValueChange={(value) => this.selectionDisplay.distance = value} />
              <Text style={styles.sliderText}>30km</Text>
            </View>
          </ScrollView>
          <TouchableOpacity onPress={() => this.props.resetFilters()} activeOpacity={0.9}>
            <View
              style={{
                height: 48,
                backgroundColor: colors.brand.primary,
                alignItems: 'center',
                justifyContent: 'center',
                elevation: 2
              }}><Text
              style={{ color: 'white' }}>RESET</Text></View>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  genderImage: {
    width: 24,
    height: 24
  },
  furnitureImage: {
    width: 28,
    height: 28
  },
  titleText: {
    color: '#616161',
    fontSize: 10,
    fontFamily: appfontFamily.semiBold600
  },
  bedImage: {
    width: 28,
    height: 28
  },
  optionDeselect: {
    marginLeft: 8,
    fontSize: 10,
    color: '#B0B3BB',
    fontFamily: appfontFamily.medium500
  },
  optionSelect: {
    marginLeft: 8,
    fontSize: 10,
    color: colors.brand.primary,
    fontFamily: appfontFamily.medium500
  },
  optionContainer: {
    flexDirection: 'row', alignItems: 'center'
  },
  sliderText: {
    borderRadius: 10,
    borderColor: 'grey',
    borderWidth: 0.2,
    paddingLeft: 4,
    paddingRight: 4,
    paddingTop: 2,
    paddingBottom: 2
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(Filters);
