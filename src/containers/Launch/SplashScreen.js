/**
 * Created by thespidy on 24/06/17.
 */
/**
 * Launch Screen
 *  - Shows a nice loading screen whilst:
 *  - Checking if user is logged in, and redirects from there
 *
 * React Native Starter App
 * https://github.com/mcnamee/react-native-starter-app
 */
import React, { Component } from 'react';
import {
  View,
  Image,
  AsyncStorage,
} from 'react-native';

import SplashScreen from 'react-native-splash-screen'


import { Actions } from 'react-native-router-flux';

import * as UserActions from '@redux/user/actions';

import store from 'src/store';

class LaunchScreen extends Component {
  static componentName = 'AppLaunch';

  componentWillMount = async() => {
    SplashScreen.hide();
    let {navigate} = this.props.navigation;
    let user = await AsyncStorage.getItem('user');
    user = user ? JSON.parse(user) : user;
    if (!user || !user.accessToken) {
      navigate('Auth');
    } else {
      store.dispatch(UserActions.setUserDetails(user));
      navigate('Main');
    }
  };

  render = () => (
    <View style={{ flex: 1, width: null, height: null }}>
    </View>
  );
}

export default LaunchScreen;
