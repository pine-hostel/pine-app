/**
 * Tabbar Icon
 *
 <TabIcon icon={'search'} selected={false} />
 *
 * React Native Starter App
 * https://github.com/mcnamee/react-native-starter-app
 */
import React, {PropTypes} from 'react';

import {View, Image, Text} from 'react-native';

import {createIconSetFromFontello} from 'react-native-vector-icons';
import config from '../../BrewwyFont/config.json';
//let Icon = createIconSetFromFontello(config);
import Icon from 'react-native-vector-icons/FontAwesome';

import {AppColors} from '@theme/';

class TabIcon extends React.Component {
    render() {
        let icon;
        switch (this.props.src) {
            case 'Home':
                icon = require('./images/home.png');
                break;
            case 'My List':
                icon = require('./images/list.png');
                break;
            case 'Trending':
                icon = require('./images/trending.png');
                break;
            case 'Profile':
                icon = require('./images/profile.png');
                break;
            case 'Notification':
                icon = require('./images/notification_bell.png');
                break;
        }
        return (
            <View style={{flex:1, justifyContent:"center", alignItems:"center"}}>
                <Image source={icon} style={{width:20, height:20}} resizeMode={'contain'}/>
                <Text style={{fontSize:9, color: "#000000"}}>{this.props.src}</Text>
            </View>
        );
    }
}

TabIcon.propTypes = {
    //src: React.PropTypes.string,
    size: React.PropTypes.number,
    color: React.PropTypes.string,
    styles: React.PropTypes.object
}

export default TabIcon;
