/**
 * Created by thespidy on 06/08/17.
 */

import React, { PropTypes } from 'react';
import { View, Text } from 'react-native';

const Chip = ({ count, label }) => (
  <View
    style={{
       flexDirection: 'row',
       height:24,
       alignItems: 'center',
       borderRadius: 20,
       borderColor: 'grey',
       borderWidth:0.8,
       backgroundColor:'white'
    }}
  >
    <Text
      style={{width:24, height:24, borderRadius: 40, borderColor:'grey', backgroundColor:'grey', color:'white', textAlign:'center'}}>{count}</Text>
    <Text style={{color:'black', padding:4}}>{label}</Text>
  </View>
);
export default Chip;