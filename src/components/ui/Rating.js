/**
 * Created by thespidy on 09/07/17.
 */
import React, { PropTypes } from 'react';
import { View, Text } from 'react-native';
import { Icon } from 'react-native-elements'

function getRatingStars(rating) {
  let icons = [];
  let i=1
  for(i; i<= rating;i++) {
    icons.push(<Icon name={'star'} color={'orange'} size={14} />);
  }
  if(rating > i-1) {
    icons.push(<Icon name={'star-half'} color={'orange'} size={14} />);
    i++;
  }
  for(i; i<= 5;i++) {
    icons.push(<Icon name={'star-border'} color={'orange'} size={14} />);
  }
  return icons;
}
const Rating = ({ rating, count }) => (
  <View
    style={{
      flexDirection: 'row'
    }}
  >
    {getRatingStars(rating)}
    <Text style={{paddingLeft:8, color:'white', backgroundColor: 'transparent' }}>({count})</Text>
  </View>
);

Rating.propTypes = { rating: PropTypes.number, count: PropTypes.number };
Rating.defaultProps = { rating: 0, count: 0};
Rating.componentName = 'Rating';

/* Export Component ==================================================================== */
export default Rating;