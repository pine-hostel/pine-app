import AppAPI from '@lib/api';
import store from 'src/store';

export function getHostels(filters) {
  return (dispatch) => {
    filters = filters || {};
    let address = store.getState().hostels.place.addressObj;
    let filterParams = { ...filters };
    for(let item in filterParams) {
        if(filterParams[item] instanceof Array) {
          filterParams[item] = filterParams[item].join();
        }
    }
    let params = { ...filterParams, ...address };
    AppAPI.hostels.get(params)
      .then((result) => {
        console.log(result);
        dispatch({
          type: 'HOSTELS_SUCCESS',
          data: result
        });
      }, err => dispatch({ type: 'HOSTELS_ERROR', data: err.message }));
  }
}

export function setDistanceInHostels(data) {
  return {
    type: 'SET_DISTANCE_IN_HOSTELS',
    data: data
  }
}

export function setFilters(data) {
  return {
    type: 'SET_FILTERS',
    data: data
  }
}

export function resetFilters(data) {
  return {
    type: 'RESET_FILTERS'
  }
}

export function setPlace(data) {
  return {
    type: 'SET_PLACE',
    data: data
  }
}