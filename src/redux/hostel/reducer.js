import { AsyncStorage } from 'react-native';

let defaultFilters = {
  tenantType_in: [],
  bedType_in: [],
  furnishingType_in: [],
  accomodationType_in: [],
  startsFrom_max: 20000,
  radius: 30,
  rating_min: 1
};

let initialState = {
  isLoading: false, data: [], markers: [], filters: defaultFilters,
  place: { addressObj: {}}
};
export default function hostelRenderer(state = initialState, action) {
  switch (action.type) {
    case 'FETCH_HOSTELS_START':
      return {
        ...state,
          isLoading: true
      };
    case 'HOSTELS_SUCCESS':
      return {
        ...state,
          isLoading: false,
          data: action.data.list,
          markers: action.data.markers
      };
    case 'ADD_FAVOURITES_SUCCESS':
      let hostelsList = state.data;
      for(let hostel of hostelsList) {
        if(hostel._id === action.data._id) {
          hostel.isFavourite = action.data.isFavourite;
        }
      }
      return {
        ...state
      };
    case 'FETCH_HOSTELS_FAILURE':
      return {
        ... state,
        isLoading: false,
        error: "some error occured, please try again"
      };
    case 'SET_DISTANCE_IN_HOSTELS': {
      let hostelsData = state.data;
      for (let i = 0; i < action.data.length; i++) {
        hostelsData[i].direction = action.data[i];
      }
      return {
        ...state,
        data: hostelsData
      };
    }
    case 'SET_FILTERS': {
      return {
        ...state,
        filters: action.data
      };
    }
    case 'RESET_FILTERS': {
      return {
        ...state,
        filters: defaultFilters
      };
    }
    case 'SET_PLACE': {
      return {
        ...state,
        place: action.data
      };
    }
    default:
      return state;
  }
}
