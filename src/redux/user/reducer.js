import { AsyncStorage } from 'react-native';
export default function userReducer(state = {}, action) {
  switch (action.type) {
    case 'LOGIN_SUCCESS':
      let user = {
        firstName: action.data.firstName,
        lastName: action.data.lastName,
        email: action.data.email,
        username: action.data.name,
        profilePicUrl: action.data.profilePicUrl,
        phone: action.data.phone,
        accessToken: action.data.accessToken
      };
      try {
        AsyncStorage.setItem('user', JSON.stringify(user));
        AsyncStorage.setItem('accessToken', action.data.accessToken);
      } catch (error) {
        console.log("Error saving using AsyncStorage", error);
      }
      return {
        ...user
      };

    case 'SET_USER_DETAILS':
      return {
        ...state,
        ... action.data
      };

    case 'USER_UPDATE_SUCCESS':
      user = { ...state, ...action.data };
      console.log(user);
      try {
        AsyncStorage.setItem('user', JSON.stringify(user));
      } catch (error) {
        console.log("Error saving using AsyncStorage", error);
      }
      return user;
    case 'SET_USER_LOCATION':
      return {
        ...state,
        location: action.data
      };
    case 'LOGIN_OTP_SUCCESS':
      return {
        ...state,
        otp: action.data.otp
      };
    case 'LOGOUT':
      AsyncStorage.removeItem('user');
      AsyncStorage.removeItem('accessToken');
      return {};
    default:
      return state;
  }
}
