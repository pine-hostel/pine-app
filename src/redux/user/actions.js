import AppAPI from '@lib/api';

export function oauthLogin(type, data, callback) {
  return dispatch => AppAPI.oauth.post(null, { ...data, kind: type })
    .then((result) => {
      dispatch({
        type: 'LOGIN_SUCCESS',
        data: { ...result },
      });
      callback();
    }, err => callback(err));
}

export function oauthWithPhone(phone, otp, callback) {
  return dispatch => AppAPI.oauth.post(null, { phone, otp, kind: 'phone' })
    .then((result) => {
      dispatch({
        type: 'LOGIN_SUCCESS',
        data: { ...result },
      });
      callback();
    }, err => callback(err));
}

export function getLoginOTP(phone) {
  return dispatch => AppAPI.loginOtp.get({ phone })
    .then((result) => {
      dispatch({
        type: 'LOGIN_OTP_SUCCESS',
        data: { ...result },
      });
    }, err => console.log(err));
}


export function logout() {
  return {
    type: 'LOGOUT'
  };
}

export function setUserLocation(data) {
  return {
    type: 'SET_USER_LOCATION',
    data
  }
}

/**
 * Get My User Data
 */
export function getMe() {
  return dispatch => AppAPI.me.get()
    .then((userData) => {
      dispatch({
        type: 'USER_REPLACE',
        data: userData,
      });

      return userData;
    });
}

export function setUserDetails(data) {
  return {
    type: 'SET_USER_DETAILS',
    data,
  };
}

export function updateMe(payload) {
  return dispatch => AppAPI.self.put(null, payload)
    .then((userData) => {
      dispatch({
        type: 'USER_UPDATE_SUCCESS',
        data: userData,
      });

      return userData;
    }, err => console.log(err));
}
