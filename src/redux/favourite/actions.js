/**
 * Created by thespidy on 06/08/17.
 */
import AppAPI from '@lib/api';

export function getFavourites(data) {
  return (dispatch) => {
    AppAPI.favourites.get()
      .then((result) => {
        dispatch({
          type: 'GET_FAVOURITES_SUCCESS',
          data: result
        });
      }, err => dispatch({type: 'HOSTELS_ERROR', data: err.message}));
  }
}

export function addToFavourite(id) {
  return (dispatch) => {
    AppAPI.favourites.put(id)
      .then((result) => {
        dispatch({
          type: 'ADD_FAVOURITES_SUCCESS',
          data: result
        });
      }, err => dispatch({type: 'HOSTELS_ERROR', data: err.message}));
  }
}