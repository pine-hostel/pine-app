import { AsyncStorage } from 'react-native';

let initialState = {
  isLoading: false, data: []
};
export default function favouriteReducer(state = initialState, action) {
  switch (action.type) {
    case 'GET_FAVOURITES_SUCCESS':
      return {
        ...state,
          isLoading: false,
          data: action.data
      };
    case 'ADD_FAVOURITES_SUCCESS':
      let hostelsList = state.data;
      for(let i=0;i<hostelsList.length;i++) {
        if(hostelsList[i]._id === action.data._id) {
          if(!action.data.isFavourite) {
            hostelsList.splice(i, 1);
            break;
          }
        }
      }
      return {
        ...state
      };

    default:
      return state;
  }
}
