/**
 * API Config
 *
 * React Native Starter App
 * https://github.com/mcnamee/react-native-starter-app
 */

export default {
  // The URL we're connecting to
  hostname: 'http://ec2-54-191-9-89.us-west-2.compute.amazonaws.com:3002/api',

  // Map shortnames to the actual endpoints, so that we can
  // use them like so: AppAPI.ENDPOINT_NAME.METHOD()
  //  NOTE: They should start with a /
  //    eg.
  //    - AppAPI.recipes.get()
  //    - AppAPI.users.post()
  //    - AppAPI.favourites.patch()
  //    - AppAPI.blog.delete()
  endpoints: new Map([
    ['oauth', '/oauth/token'], // If you change the key, update the reference below
    ['users', '/users'],
    ['self', '/self'],
    ['hostels', '/hostels'],
    ['favourites', '/favourites'],
    ['myOrders', '/my-orders'],
    ['loginOtp', '/oauth/login-otp']
  ]),

  // Which 'endpoint' key deals with our tokens?
  tokenKey: 'oauth',
};
