/**
 * Created by thespidy on 24/06/17.
 */

/* global __DEV__ */
import logger from 'redux-logger';
import thunk from 'redux-thunk';
import { applyMiddleware, compose, createStore } from 'redux';
import Analytics from '@lib/analytics';

// All redux reducers (rolled into one mega-reducer)
import rootReducer from '@redux/index';


// Load middleware
let middleware = [
  Analytics,
  thunk, // Allows action creators to return functions (not just plain objects)
];

if (__DEV__) {
  // Dev-only middleware
  middleware = [
    ...middleware,
    logger(), // Logs state changes to the dev console
  ];
}

// Init redux store (using the given reducer & middleware)
export default store = compose(
  applyMiddleware(...middleware),
)(createStore)(rootReducer);