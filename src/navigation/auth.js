/**
 * Auth Scenes
 *
 * React Native Starter App
 * https://github.com/mcnamee/react-native-starter-app
 */
import React from 'react';
import { Scene } from 'react-native-router-flux';

import { AppConfig } from '@constants/';

import Authenticate from '@containers/auth/AuthenticateView';
import EnterOTP from '@containers/auth/EnterOTP';

/* Routes ==================================================================== */
const scenes = (
  <Scene key={'authenticate'}>
    <Scene
      hideNavBar
      key={'authLanding'}
      component={Authenticate}
      analyticsDesc={'Authenticate: Authentication'}
    />
    <Scene
      hideNavBar
      key={'enterOTP'}
      component={EnterOTP}
      analyticsDesc={'Authenticate: Authentication'}
    />
  </Scene>
);

export default scenes;
