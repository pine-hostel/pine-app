/**
 * Tabs Scenes
 *
 * React Native Starter App
 * https://github.com/mcnamee/react-native-starter-app
 */
import React from 'react';
import { Scene, ActionConst } from 'react-native-router-flux';

// Consts and Libs
import { AppConfig } from '@constants/';
import { AppStyles, AppSizes } from '@theme/';

// Components
import { TabIcon } from '@ui/';
import { NavbarMenuButton } from '@containers/ui/NavbarMenuButton/NavbarMenuButtonContainer';

// Scenes
import Placeholder from '@components/general/Placeholder';
import Listing from '@containers/Home/Listing';
import Favourites from '@containers/Home/Favourites';
import ChooseLocation from '@containers/Home/ChooseLocation';
import HostelDetail from '@containers/Home/HostelDetail'
import BirdView from '@containers/Home/BirdView'
import HostelDetailMapView from '@containers/Home/MapView'
import Filters from '@containers/Home/Filters'
import BookForm from '@containers/Home/BookForm'
import Profile from '@containers/profile/Profile'
import ProfileEdit from '@containers/profile/ProfileEdit'
import colors from '@theme/colors';

const navbarPropsTabs = {
  ...AppConfig.navbarProps,
  sceneStyle: {
    ...AppConfig.navbarProps.sceneStyle
  },
};

const navbarGrey = {
  ...navbarPropsTabs,
  navigationBarStyle: {
    ...navbarPropsTabs.navigationBarStyle,
    backgroundColor: '#2A323A'
  }
}

const navbarOrange = {
  ...AppConfig.navbarProps,
  navigationBarStyle: {
    ...AppConfig.navbarProps.navigationBarStyle,
    backgroundColor: colors.brand.primary
  }
}

const navigationMenu = {
  ...navbarPropsTabs,
  renderBackButton: () => <NavbarMenuButton />,
  navigationBarStyle: {
    ...AppConfig.navbarProps.navigationBarStyle,
    backgroundColor: '#2A323A'
  }
}

/* Routes ==================================================================== */
const scenes = (
  <Scene key={'main'} panHandlers={null}>
    <Scene
      hideNavBar
      key={'chooseLocation'}
      component={ChooseLocation}
      title={''}
    />
    <Scene
      {...navigationMenu}
      key={'listing'}
      title={'PG/Hostels'}
      titleStyle={{color: 'white', alignSelf: 'center'}}
      component={Listing} />

    <Scene
      {...navigationMenu}
      key={'favourites'}
      title={'Favourites'}
      titleStyle={{color: 'white', alignSelf: 'center'}}
      component={Favourites} />

    <Scene
      {...navigationMenu}
      key={'birdView'}
      title={'Map View'}
      titleStyle={{color: 'white', alignSelf: 'center'}}
      component={BirdView} />

    <Scene
      key={'notification'}
      {...navbarPropsTabs}
      title={'Alerts'}
      component={Placeholder}
    />
    <Scene
      {...navigationMenu}
      key={'profile'}
      component={Profile}
    />
    <Scene
      {...navbarGrey}
      key={'profileEdit'}
      component={ProfileEdit}
    />
    <Scene
      {...navbarGrey}
      key={'hostelDetails'}
      component={HostelDetail}
    />

    <Scene
      {...navbarGrey}
      key={'bookForm'}
      title={'Booking'}
      titleStyle={{color: 'white', alignSelf: 'center'}}
      component={BookForm}
    />

    <Scene
      {...navbarOrange}
      title={'Filters'}
      titleStyle={{color: 'white', alignSelf: 'center'}}
      key={'filters'}
      component={Filters}
    />

    <Scene
      {...navbarGrey}
      key={'hostelDetailMap'}
      component={HostelDetailMapView}
    />
  </Scene>
);

export default scenes;
