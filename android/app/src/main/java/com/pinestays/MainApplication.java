package com.pinestays;

import android.app.Application;

import com.facebook.CallbackManager;
import com.facebook.FacebookSdk;
import com.facebook.react.ReactApplication;
import com.airbnb.android.react.maps.MapsPackage;
import co.apptailor.googlesignin.RNGoogleSigninPackage;
import com.arttitude360.reactnative.rngoogleplaces.RNGooglePlacesPackage;
    import com.showlocationservicesdialogbox.LocationServicesDialogBoxPackage;
import com.airbnb.android.react.maps.MapsPackage;
import com.arttitude360.reactnative.rngoogleplaces.RNGooglePlacesPackage;
import com.cboy.rn.splashscreen.SplashScreenReactPackage;
import com.mustansirzia.fused.FusedLocationPackage;
import com.showlocationservicesdialogbox.LocationServicesDialogBoxPackage;
import com.bugsnag.BugsnagReactNative;

import co.apptailor.googlesignin.RNGoogleSigninPackage;

import com.facebook.reactnative.androidsdk.FBSDKPackage;
import com.oblador.vectoricons.VectorIconsPackage;
import com.idehub.GoogleAnalyticsBridge.GoogleAnalyticsBridgePackage;
import com.learnium.RNDeviceInfo.RNDeviceInfo;
import com.facebook.react.ReactNativeHost;
import com.facebook.react.ReactPackage;
import com.facebook.react.shell.MainReactPackage;
import com.facebook.soloader.SoLoader;

import java.util.Arrays;
import java.util.List;

public class MainApplication extends Application implements ReactApplication {

    private static CallbackManager mCallbackManager = CallbackManager.Factory.create();

    private final ReactNativeHost mReactNativeHost = new ReactNativeHost(this) {
        @Override
        public boolean getUseDeveloperSupport() {
            return BuildConfig.DEBUG;
        }

        @Override
        protected List<ReactPackage> getPackages() {
            return Arrays.<ReactPackage>asList(
                    new MainReactPackage(),
            new MapsPackage(),
            new RNGoogleSigninPackage(),
            new RNGooglePlacesPackage(),
                    new LocationServicesDialogBoxPackage(),
                    new SplashScreenReactPackage(),
                    new FusedLocationPackage(),
                    BugsnagReactNative.getPackage(),
                    new FBSDKPackage(mCallbackManager),
                    new VectorIconsPackage(),
                    new GoogleAnalyticsBridgePackage(),
                    new RNDeviceInfo()
            );
        }
    };

    @Override
    public ReactNativeHost getReactNativeHost() {
        return mReactNativeHost;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        FacebookSdk.sdkInitialize(getApplicationContext());
        SoLoader.init(this, /* native exopackage */ false);
    }

    protected static CallbackManager getCallbackManager() {
        return mCallbackManager;
    }
}
