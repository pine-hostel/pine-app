/**
 * Load the App component.
 *  (All the fun stuff happens in "/src/index.js")
 *
 * React Native Starter App
 * https://github.com/mcnamee/react-native-starter-app
 */

import { AppRegistry } from 'react-native';
import AppContainer from './src/';

import { Client } from 'bugsnag-react-native';
import SplashScreen from 'react-native-splash-screen'
const bugsnag = new Client();


AppRegistry.registerComponent('PineStays', () => AppContainer);
